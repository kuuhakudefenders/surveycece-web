@extends('layout')

@section('head')

@stop

@section('content')
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
<div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
    <div class="row">
        <h1>Developer Resourses</h1>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead class="thead-inverse">
                <tr>
                    <th> Nome </th>
                    <th> Ações </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> Avaliações </td>
                    <td> 
                        <a id="edit-btn" class="btn btn-default" href="{{ URL::to('avaliacaos/') }}"><i class="fa fa-book " aria-hidden="true"></i> Detalhes</a>
                    </td>
                </tr>
                <tr>
                    <td> Cursos </td>
                    <td> 
                        <a id="edit-btn" class="btn btn-default" href="{{ URL::to('cursos/') }}"><i class="fa fa-book " aria-hidden="true"></i> Detalhes</a>
                    </td>
                </tr>
                <tr>
                    <td> Disciplinas </td>
                    <td> 
                        <a id="edit-btn" class="btn btn-default" href="{{ URL::to('disciplinas/') }}"><i class="fa fa-book " aria-hidden="true"></i> Detalhes</a>
                    </td>
                </tr>
                <tr>
                    <td> Perguntas </td>
                    <td> 
                        <a id="edit-btn" class="btn btn-default" href="{{ URL::to('perguntas/') }}"><i class="fa fa-book " aria-hidden="true"></i> Detalhes</a>
                    </td>
                </tr>
                <tr>
                    <td> Professores </td>
                    <td> 
                        <a id="edit-btn" class="btn btn-default" href="{{ URL::to('professors/') }}"><i class="fa fa-book " aria-hidden="true"></i> Detalhes</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
@stop

@section('scripts')
@stop