@extends('layout')

@section('head')
<link rel="stylesheet" type="text/css" href="css/event-new.css" />
<script type="text/javascript" src="../js/events-home.js"></script>
@stop

@section('content')
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    <h1>Novo Evento</h1>
    <br>
    <form class="form-horizontal" id="new-event">
      <div class="form-group required">
        <label class="control-label col-sm-3" for="new-event-name">Nome:</label>
        <div class="col-sm-9">
          <input class="form-control" id="new-event-name" placeholder="Insira o nome do evento">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-3" for="new-event-descricao">Descrição:</label>
        <div class="col-sm-9">
            <textarea class="form-control"  form = "new-event" id="new-event-descricao" name="new-event-descricao" placeholder="Insira a descrição do evento"  cols="35" wrap="soft"></textarea>
            <!--
            <input class="form-control" id="new-event-descricao" placeholder="Insira a descrição do evento">
            -->
        </div>
      </div>
      <div class="form-group required">
        <label class="control-label col-sm-3" for="new-event-date">Data de início:</label>
        <div class="col-sm-2">
          <select class="form-control" id="new-event-day">
            <option>Dia</option>
            <option>01</option>
            <option>02</option>
            <option>03</option>
            <option>04</option>
            <option>05</option>
            <option>06</option>
            <option>07</option>
            <option>08</option>
            <option>09</option>
            <option>10</option>
            <option>11</option>
            <option>12</option>
            <option>13</option>
            <option>14</option>
            <option>15</option>
            <option>16</option>
            <option>17</option>
            <option>18</option>
            <option>19</option>
            <option>20</option>
            <option>21</option>
            <option>22</option>
            <option>23</option>
            <option>24</option>
            <option>25</option>
            <option>26</option>
            <option>27</option>
            <option>28</option>
            <option>29</option>
            <option>30</option>
            <option>31</option>
          </select>
        </div>
        <div class="col-sm-2">
          <select class="form-control" id="new-event-month">
            <option>Mês</option>
            <option>01</option>
            <option>02</option>
            <option>03</option>
            <option>04</option>
            <option>05</option>
            <option>06</option>
            <option>07</option>
            <option>08</option>
            <option>09</option>
            <option>10</option>
            <option>11</option>
            <option>12</option>
          </select>
        </div>
        <div class="col-sm-2">
          <select class="form-control" style="width: 80px;" id="new-event-year">
            <!--  Favor arrumar esses input de data-->
            <option>Ano</option>
            <option>2014</option>
            <option>2015</option>
            <option>2016</option>
            <option>2017</option>
            <option>2018</option>
            <option>2019</option>
            <option>2020</option>
            <option>2021</option>
            <option>2022</option>
            <option>2023</option>
            <option>2024</option>
            <option>2025</option>
            <option>2026</option>
            <option>2027</option>
            <option>2028</option>
          </select>
        </div>
      </div>

    </form>

    <br> 
    <div class="col-sm-offset-2 col-sm-10">
      <button id="new-event-save-btn" url="{{url('eventos')}}" class="btn btn-success">Salvar</button>
      <a href="{{url('eventos')}}" class="btn btn-default">Cancelar</a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
</div>
@stop

@section('scripts')
@stop
