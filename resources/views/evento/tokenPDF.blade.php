<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
}

td, th {
    border: 1px solid #dddddd;
    text-align: center;
    padding: 10px;
}

</style>
</head>
<body>

<?php
$tokenArraySize = count($tokenArray);
$maxCOL = 6;
$COL = min($maxCOL,$tokenArraySize);

echo "<table width=".strval(100/$maxCOL * $COL)."%><tr>";

foreach ($tokenArray as $i=>$number) {
    echo "<td width=".strval(100/$maxCOL)."%>".$number."</td>";
    if( ($i + 1)%$COL == 0 ){
        echo "</tr><tr>";
    }
}
?>
</tr>
</table>

</body>
</html>
