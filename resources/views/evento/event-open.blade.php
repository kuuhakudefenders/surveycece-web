@extends('layout')

@section('head')
<link rel="stylesheet" type="text/css" href="../css/event-open.css" />
<script type="text/javascript" src="../js/event-open.js"></script>
<script src="holder.js"></script>
@stop

@section('content')
<div id="event-show">
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
  </div>
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" >
      <div class="col-sm-offset-5 col-sm-10">
          <img src="/img/loading.gif" id="loading-indicator" style="display:none"/>
      </div>
      <br>
      <div>
        <h1>{{ $evento->nome }}</h1>
      </div>
      <div id="event-id" class="invisible">{{ $evento->id }}</div>
      <div id="courses-url" url="{{url('cursosList')}}" class="invisible"></div>
      <div id="form-url" url="{{url('formularios')}}" class="invisible"></div>
      <div id="token-url-gen" url="{{url('/tokens/store/')}}"></div>
      <div id="token-url-dow" url="{{url('/tokens/show/')}}"></div>
      <br>
      <form class="form-horizontal">
        <div id="event-infos-show" class="form-group">

          <div class="row">
            <label class="control-label col-xs-2 col-sm-2 col-md-2 col-lg-2" for="open-date">Data de Início</label>
            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
              <p id="open-date" class="form-control-static">{{dateFormatBrazil($evento->dt_inicio)}}</p>
            </div>
            @if ($evento->abertoSN === 1)
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="float: right; margin-right: 40px">
              <a id="edit-btn" class="btn btn-default"><i class="fa fa-cog" aria-hidden="true"></i> Editar</a>
            </div>
            @endif
          </div>
          
          <div class="row">
            <label class="control-label col-xs-2 col-sm-2 col-md-2 col-lg-2" for="closed-date">Data de Fim</label>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              <p id="closed-date" class="form-control-static">{{ dateFormatBrazil($evento->dt_fim) }}</p>
            </div>
          </div>
        
          <div class="row">
            <label class="control-label col-xs-2 col-sm-2 col-md-2 col-lg-2" for="situation">Situação</label>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              @if ($evento->abertoSN === 1)
                <p class="form-control-static">Aberto</p>
              @else
                <p class="form-control-static">Fechado</p>
              @endif
            </div>
          </div>

          <div class="row">
            <label class="control-label col-xs-2 col-sm-2 col-md-2 col-lg-2" for="closed-date">Descrição</label>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              <p class="form-control-static">{{ $evento->descricao }}</p>
            </div>
          </div>

        </div>
      </form>
      <br>
      <div id="form-panel" class="panel panel-default">
          <div id="add-form-btn" class="panel-heading">Formulários 
            @if ($evento->abertoSN === 1)
            <button class="btn btn-xs btn-primary" style="float:right">
              <i class="fa fa-plus" aria-hidden="true"></i></button>  
            @endif
            </div>
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Curso</th>
                  <th>Situação</th>
                  <th>Link do Formulário</th>
                  <th>Ação</th>
                </tr>
              </thead>
              
              <tbody>
                @foreach($evento->formularios as  $formulario)
                  @foreach($formulario->curso as  $curso)
                <tr>
                  <td>{{ $curso->nome }}</td>
                  @if ($formulario->tokenGeradoSN === 0)
                  <td>Aberto</td>
                  <td>
                    @if ($formulario->linkGoogleForm === NULL && $evento->abertoSN === 1)
                      <button form-id="{{ $formulario->id }}" class="btn btn-sm btn-primary btn-add-link"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</button>
                    @elseif ($formulario->linkGoogleForm === NULL && $evento->abertoSN === 0)
                      
                    @else
                      <a href="{{ $formulario->linkGoogleForm }}" target="_blank">Acessar</a>
                    @endif
                  </td>
                  <td>
                    @if ($evento->abertoSN === 1)
                    <button form-id="{{ $formulario->id }}" class="btn btn-sm btn-warning btn-gen-token"><i class="fa fa-lock" aria-hidden="true"></i> Gerar Senhas</button>
                    <button form-id="{{ $formulario->id }}" class="btn btn-sm btn-danger btn-rm-form"><i class="fa fa-times" aria-hidden="true"></i> Remover</button>
                    @endif
                  </td>
                  @elseif ($formulario->validadoSN === 0)
                  <td>Senhas geradas</td>
                  <td>
                    @if ($formulario->linkGoogleForm === NULL && $evento->abertoSN === 1)
                      <button form-id="{{ $formulario->id }}" class="btn btn-sm btn-primary btn-add-link"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</button>
                    @elseif ($formulario->linkGoogleForm === NULL && $evento->abertoSN === 0)
                    
                    @else
                      <a href="{{ $formulario->linkGoogleForm }}" target="_blank">Acessar</a>
                    @endif
                  </td>
                  <td>
                    @if ($evento->abertoSN === 1)
                    <button form-id="{{ $formulario->id }}" class="btn btn-sm btn-success btn-validate"><i class="fa fa-check" aria-hidden="true"></i> Validar</button>
                    <button form-id="{{ $formulario->id }}" class="btn btn-sm btn-danger btn-rm-form"><i class="fa fa-times" aria-hidden="true"></i> Remover</button>
                    @endif
                    <a href="{{url('/tokens/show/').'/'. $formulario->id}}" target="_blank"><button form-id="{{ $formulario->id }}" class="btn btn-sm btn-success btn-dow-token" style="float:right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Baixar Senhas</button></a>
                  </td>
                  @else
                  <td>Validado</td>
                  <td>
                    @if ($formulario->linkGoogleForm === NULL && $evento->abertoSN === 1)
                      <button form-id="{{ $formulario->id }}" class="btn btn-sm btn-primary btn-add-link"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</button>
                    @elseif ($formulario->linkGoogleForm === NULL && $evento->abertoSN === 0)
                      
                    @else
                      <a href="{{ $formulario->linkGoogleForm }}" target="_blank">Acessar</a>
                    @endif
                  </td>
                  <td>
                    @if ($evento->abertoSN === 1)
                    <button form-id="{{ $formulario->id }}" class="btn btn-sm btn-danger btn-rm-form"><i class="fa fa-times" aria-hidden="true"></i> Remover</button>
                    @endif
                    <a href="{{url('validate-responses/download/').'/'. $formulario->id}}" target="_blank"><button form-id="{{ $formulario->id }}" class="btn btn-sm btn-success btn-dow-ans" style="float:right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Baixar Respostas Validadas</button></a>
                  </td>
                  @endif
                </tr>
                <!--
                <tr>
                  <td>EE</td>
                  <td>Senhas geradas</td>
                  <td>
                    <button class="btn btn-sm btn-success"><i class="fa fa-check" aria-hidden="true"></i> Validar</button>
                    <button class="btn btn-sm btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Remover</button>
                  </td>
                </tr>
                <tr>
                  <td>M</td>
                  <td>Validado</td>
                  <td>
                    <button class="btn btn-sm btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Remover</button>
                  </td>
                </tr>
              -->
                @endforeach
              @endforeach
              </tbody>
            </table>  
          </div>
      </div>
  </div>
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
  </div>
  
  <div id="create-form-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Criar Formulário</h4>
        </div>
        <div class="modal-body">
          <div class="row">
          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <form class="form-horizontal">
              <div class="form-group">
                <label class="control-label col-sm-2" for="edit-name">Curso:</label>
                <div class="col-sm-10">
                  <select class="form-control" id="form-courses">
                  </select>
                </div>
              </div>
            </form>
          </div>
          </div>
          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        </div>
        <div class="modal-footer">
          <button id="save-form-btn" type="button" class="btn btn-primary" data-dismiss="modal">Salvar</button>
        </div>
      </div>

    </div>
  </div>
  <div id="gen-token-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Gerar Senhas</h4>
        </div>
        <div class="modal-body">
          <div class="row">
          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <form class="form-horizontal">
              <div class="form-group">
                <label class="control-label col-sm-3" for="edit-name">Número de senhas:</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="tokens-num-input">
                </div>
              </div>
            </form>
          </div>
          </div>
          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        </div>
        <div class="modal-footer">
          <button id="gen-token-btn" type="button" class="btn btn-primary" data-dismiss="modal">Gerar</button>
        </div>
      </div>

    </div>
  </div>
  <div id="validate-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Validar Formulário</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="container-fluid">
              <div class="panel panel-default">
                <div class="panel-heading">Insira o arquivo de respostas (.csv)</div>
                <div class="panel-body">
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <form>
                      <input type="file" name="pic" id="csvResponses" data-path="{{url('validate-responses')}}">
                    </form>
                  </div>
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button id="validate-btn" type="button" class="btn btn-primary" data-dismiss="modal">Validar</button>
        </div>
      </div>

    </div>
  </div>
  <div id="add-form-link-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Adicionar Link do Google Forms</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="control-label col-sm-4" for="edit-name">Link do Formulário:</label>
                  <div class="col-sm-8">
                    <input class="form-control" id="link-google-form-input">
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        </div>
        <div class="modal-footer">
          <button id="add-link-btn" type="button" class="btn btn-primary" data-dismiss="modal">Adicionar</button>
        </div>
      </div>

    </div>
  </div>
</div>

<div id="event-edit" class="invisible">
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
  </div>
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
      <h1>Editar Evento</h1>
      <br>
      <form class="form-horizontal">
        <div class="form-group required">
          <label class="control-label col-sm-2" for="edit-name">Nome:</label>
          <div class="col-sm-10">
            <input class="form-control" id="edit-name" value="{{ $evento->nome }}">
          </div>
        </div>

        <div class="form-group required">
          <label class="control-label col-sm-2" for="edit-open-date">Data de início:</label>
          <div class="col-sm-2">
            <select class="form-control" id="edit-open-day">
              <option>Dia</option>
              <option>01</option>
              <option>02</option>
              <option>03</option>
              <option>04</option>
              <option>05</option>
              <option>06</option>
              <option>07</option>
              <option>08</option>
              <option>09</option>
              <option>10</option>
              <option>11</option>
              <option>12</option>
              <option>13</option>
              <option>14</option>
              <option>15</option>
              <option>16</option>
              <option>17</option>
              <option>18</option>
              <option>19</option>
              <option>20</option>
              <option>21</option>
              <option>22</option>
              <option>23</option>
              <option>24</option>
              <option>25</option>
              <option>26</option>
              <option>27</option>
              <option>28</option>
              <option>29</option>
              <option>30</option>
              <option>31</option>
            </select>
          </div>
          <div class="col-sm-2">
            <select class="form-control" id="edit-open-month">
              <option>Mês</option>
              <option>01</option>
              <option>02</option>
              <option>03</option>
              <option>04</option>
              <option>05</option>
              <option>06</option>
              <option>07</option>
              <option>08</option>
              <option>09</option>
              <option>10</option>
              <option>11</option>
              <option>12</option>
            </select>
          </div>
          <div class="col-sm-2">
            <select class="form-control" id="edit-open-year">
              <option>Ano</option>
              <option>2014</option>
              <option>2015</option>
              <option>2016</option>
              <option>2017</option>
              <option>2018</option>
              <option>2019</option>
              <option>2020</option>
              <option>2021</option>
              <option>2022</option>
              <option>2023</option>
              <option>2024</option>
              <option>2025</option>
              <option>2026</option>
              <option>2027</option>
              <option>2028</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="edit-description">Descrição:</label>
          <div class="col-sm-10">
            <input class="form-control" id="edit-description" value="{{ $evento->descricao }}">
          </div>
        </div>

      </form>

      <br> 
      <div class="col-sm-offset-2 col-sm-10">
        <button id="save-btn" url="{{url('eventos/' . $evento->id)}}" class="btn btn-success">Salvar</button>
        <a href="{{url('eventos/' . $evento->id)}}" class="btn btn-default">Cancelar</a>
      </div>
  </div>
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
  </div>
</div>

<?php function dateFormatBrazil($date) {  
  if ($date == NULL)
    return "";
  
  $data = explode('-', $date);
  $data = substr($data[2], 0, 2).'/'.$data[1].'/'.$data[0];
  return $data;
}
?>


@stop

@section('scripts')
@stop
