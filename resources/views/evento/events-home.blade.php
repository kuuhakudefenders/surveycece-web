@extends('layout')

@section('head')
<link rel="stylesheet" type="text/css" href="css/events-home.css" />
<script type="text/javascript" src="../js/events-home.js"></script>
@stop

@section('content')
<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
</div>
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <div class="row">
        <h1>Eventos</h1>
        <br>
    </div>
    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-centered">
        <a href="{{url('eventos/create')}}">
            <div class="card card-add">
                <i id="add-event-icon" class="fa fa-plus-circle fa-4x" aria-hidden="true"></i>
            </div>
        </a>

        @foreach($eventos as $key => $value)
            @if ($value->abertoSN === 1)
                <div class="card card-open">
                    <div class="card-container">
                        <div class="text-center">
                            <h5 style="color: #BBBBBB">Evento Aberto</h5>
                            <h4>{{ $value->nome }}</h4>
                        </div>
                        <br>
                        <div class="upper-space">
                            <a href="{{ URL::to('eventos/' . $value->id) }}">
                            <button class="btn btn-sm btn-primary full-width invisible"><i class="fa fa-eye" aria-hidden="true"></i> Ver</button>
                            </a>
                        </div>
                        <div class="upper-space">
                            <button url="{{ url('eventos/close/' . $value->id) }}" class="btn btn-sm btn-success btn-close-event full-width invisible"><i class="fa fa-check-square" aria-hidden="true"></i> Fechar</button>
                        </div>
                        <div class="upper-space">
                            <button class="btn btn-rm btn-sm btn-danger full-width invisible" url="{{url('eventos/' . $value->id)}}"> <i class="fa fa-times" aria-hidden="true"></i> Remover</button>
                        </div>                    
                    </div>
                </div>
            @else
                <!-- <a href="{{ URL::to('eventos/' . $value->id) }}"> -->
                    <div class="card card-closed">
                        <div class="card-container">
                            <div class="text-center">
                                <h5 style="color: #ABABAB">Evento Fechado</h5>
                                <h4>{{ $value->nome }}</h4>
                            </div>

                        
                            <br>
                            <div class="upper-space">
                                <a href="{{ URL::to('eventos/' . $value->id) }}">
                                <button class="btn btn-sm btn-primary full-width invisible"><i class="fa fa-eye" aria-hidden="true"></i> Ver</button>
                                </a>
                            </div>
                        </div>
                    </div>
                <!-- </a> -->
            @endif
        @endforeach
        <div id="close-event-modal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Fechar Evento</h4>
              </div>
              <div class="modal-body">
                <p>Tem certeza que deseja fechar este evento?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button id="confirm-close-event" type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
              </div>
            </div>

          </div>
        </div>
    </div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
</div>
@stop

@section('scripts')
@stop
