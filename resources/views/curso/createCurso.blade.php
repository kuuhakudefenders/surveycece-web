@extends('layout')

@section('head')
<script type="text/javascript" src="../js/cursos.js"></script>
@stop

@section('content')
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    <h1>Novo Curso</h1>
    <br>
    <form class="form-horizontal" id="new-curso">
      <div class="form-group required">
        <label class="control-label col-sm-3" for="new-curso-name">Nome:</label>
        <div class="col-sm-9">
          <input class="form-control" id="new-curso-name" placeholder="Insira o nome do curso">
        </div>
      </div>
    </form>

    <br> 
    <div class="col-sm-offset-2 col-sm-10">
      <button id="new-curso-save-btn" url="{{url('cursos')}}" class="btn btn-success">Salvar</button>
      <a href="{{url('cursos')}}" class="btn btn-default">Cancelar</a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
</div>
@stop

@section('scripts')
@stop
