@extends('layout')

@section('head')

@stop

@section('content')
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
<div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
    <div class="row">
        <h1>Cursos</h1>
    </div>
    <div class="row">
        <a id="edit-btn" class="btn btn-default" href="{{ URL::to('cursos/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</a>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead class="thead-inverse">
                <tr>
                    <th> Nome </th>
                    <th> Ações </th>
                </tr>
            </thead>
            <tbody>
                @foreach($cursos as $key => $value)
                    <tr>
                        <td> {{$value->nome}} </td>
                        <td> 
                            <a id="edit-btn" class="btn btn-default" href="{{ URL::to('cursos/' . $value->id) }}"><i class="fa fa-book " aria-hidden="true"></i> Detalhes</a>
                            <a id="edit-btn" class="btn btn-default" href="{{ URL::to('cursos/' . $value->id . '/edit') }}"><i class="fa fa-cog" aria-hidden="true"></i> Editar</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
@stop

@section('scripts')
@stop