@extends('layout')

@section('head')

@stop

@section('content')
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
<div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
    <div class="row">
        <h1>{{$curso->nome}}</h1>
    </div>
    <div class="row">
        <h2>Disciplinas</h2>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead class="thead-inverse">
                <tr>
                    <th> Disciplina </th>
                    <th> Professor </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($curso->disciplinas as $key => $disciplina)
                    <tr>
                        <td> {{$disciplina->nome}} </td>
                        <td> {{$disciplina->professor->nome}} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
@stop

@section('scripts')
@stop