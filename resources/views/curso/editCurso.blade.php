@extends('layout')

@section('head')
<script type="text/javascript" src="../../js/cursos.js"></script>
@stop

@section('content')
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
    <h1>Editar Curso</h1>
    <br>
    <form class="form-horizontal">
      <div class="form-group required">
        <label class="control-label col-sm-2" for="edit-name">Nome:</label>
        <div class="col-sm-10">
          <input class="form-control" id="edit-name" value="{{ $curso->nome }}">
        </div>
      </div>
    </form>

    <br> 
    <div class="col-sm-offset-2 col-sm-10">
      <button id="edit-curso-save-btn" url="{{url('cursos/' . $curso->id)}}" class="btn btn-success">Salvar</button>
      <a href="{{url('cursos/')}}" id="cancel-btn" class="btn btn-default">Cancelar</a>
    </div>
</div>
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
@stop

@section('scripts')
@stop