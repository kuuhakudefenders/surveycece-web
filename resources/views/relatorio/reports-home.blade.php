@extends('layout')

@section('head')
<link rel="stylesheet" type="text/css" href="../../css/reports-home.css" />
<script type="text/javascript" src="../../js/reports-home.js"></script>
<link rel="stylesheet" type="text/css" href="../../../css/reports-home.css" />
<script type="text/javascript" src="../../../js/reports-home.js"></script>
<link rel="stylesheet" type="text/css" href="../../../../../css/reports-home.css" />
<script type="text/javascript" src="../../../../../js/reports-home.js"></script>
<!--
    Wat is dat
-->
@stop

@section('content')
<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
</div>
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <div class="row">
        <h1>Relatórios</h1>
        <br>
        <!-- Eventos -->
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">Eventos</div>
                <div class="panel-body fixed-panel">
                  <ul class="nav nav-pills nav-stacked">
                    @if(isset($listEventoFechado))
                        @foreach($listEventoFechado as $key => $value)
                            <li><a href="{{ URL::to('reports/eventos/' . $value->id) }}" class="event-pill"> {{$value->nome }} </a></li>
                        @endforeach
                    @endif
                  </ul>
                </div>
            </div>
        </div>
        <!-- Cursos -->
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">Cursos</div>
                <div class="panel-body fixed-panel">
                  <ul class="nav nav-pills nav-stacked">
                    @if(isset($cursosList))
                        @foreach($cursosList as $key => $value)
                            <li><a href="{{ URL::to('reports/eventos/' . $eventoSelecionado . '/cursos/' . $value->id) }}" class="course-pill"> {{$value->nome }} </a></li>
                        @endforeach
                    @endif
                  </ul>
                </div>
            </div>
        </div>
        <!-- Relatórios -->
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">Relatórios</div>
                <div class="panel-body fixed-panel">
                  <ul class="nav nav-pills nav-stacked">
                    @if(isset($professorsList))
                        @foreach($professorsList as $key => $value)
                            <li>
                                {{$value->nome }}
                                <a href="{{ URL::to('reports/eventos/' . $eventoSelecionado . '/cursos/' . $cursoSelecionado. '/professors/'. $value->id . '/downloadRelatorioGeral') }}" class="report-pill"> Relatório Geral </a>
                                
                                <a href="{{ URL::to('reports/eventos/' . $eventoSelecionado . '/cursos/' . $cursoSelecionado. '/professors/'. $value->id . '/downloadRelatorioEspecifico') }}" class="report-pill"> Relatório Especifico </a>
                            </li>
                        @endforeach
                    @endif  
                  </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
</div>  
@stop

@section('scripts')
@stop