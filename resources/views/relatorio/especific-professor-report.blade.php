<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style type="text/css">
        	/*span {
        	  	width: 180px !important;
        	  	text-align: left !important;
        	}​*/
        	.text-rotate {
        	     -moz-transform: rotate(90deg);  /* FF3.5+ */
        	       -o-transform: rotate(90deg);  /* Opera 10.5 */
        	  -webkit-transform: rotate(90deg);  /* Saf3.1+, Chrome */
        			  transform: rotate(90deg);
        	}
        	.paragraph {
        		margin-left: 60px;
        	}
        	h4 {
        		font-family: Arial !important;
        	}
        </style>
	</head>
	<body>
		<div class="row">
		    <img src="../public/img/unioeste-logo.png" alt="logo unioeste" style="width:55px;height:40px;float: left">
		    <p class="text-center" style="vertical-align: middle;margin-right: 55px;">UNIVERSIDADE ESTADUAL DO OESTE DO PARANÁ / CAMPUS FOZ DO IGUAÇU<br>CENTRO DE ENGENHARIAS E CIÊNCIAS EXATAS</p>
		</div>

		<br><br>

		<div class="row">
			<h1 class="text-center">Relatório Especifico de Avaliação Docente</h1>
		</div>

		<br><br>

		<div class="row">
			<h2 class="paragraph">Informações da Avaliação</h2>
		</div>
		<br>

		<div class="block">
			<span for="evaluation-date"><b>Período da Avaliação:</b></span>
			<span id="evaluation-date">&nbsp;{{dateFormatBrazil($evento->dt_inicio)}} a {{dateFormatBrazil($evento->dt_inicio)}}.</span>
		</div>
		<div class="block">
			<span for="course"><b>Curso:</b></span>
			<span id="course">&nbsp;{{$curso->nome}}.</span>
		</div>
		<div class="block">
			<span for="description"><b>Descrição:</b></span>
			@if ($evento->descricao != "")
				<span id="description">&nbsp;{{$evento->descricao}}.</span>
			@else
				<span id="description">&nbsp;Nenhuma descrição fornecida.</span>
			@endif 
		</div>

		<br><br>

		<!-- <div class="text-rotate">
		oieeeeee
		</div> -->
		<div class="row">
			<h2 class="paragraph">Questões Abordadas</h2>
		</div>
		<br>
		<div class="row">
			<ol>
			@foreach($questions as $key => $value)
				<li> {{$value->nome}}. </li>
			@endforeach	
			</ol>
		</div>

		<br><br>

		<div class="row">
			<h2 class="paragraph">Estatisticas de Avaliação de Cada Disciplina</h2>
		</div>
		<br>
		
		<div class="row">
			<input type="hidden" id="rowIndex" name="rowIndex" value="0">
			@foreach($avaliacaos as $keyA => $avaliacao)
			<h3 class="paragraph">{{$avaliacao->disciplina->nome}}</h3>
			<table class="table table-striped">
			    <thead>
			    	<tr>
				    	<th class="text-center"> Pergunta</th>
				    	<th class="text-center"> Total</th>
				    	<th class="text-center"> Média</th>
				    	<th class="text-center"> Máximo</th>
				    	<th class="text-center"> Mínimo</th>
				    	<th class="text-center"> Moda</th>
				    	<th class="text-center"> Desvio Padrão</th>
				    	<th class="text-center"> Variança</th>
				    </tr>
			    </thead>
			    <tbody>
			    	@foreach($avaliacao->respostaEstatisticas as $key => $statistics)
			    		<tr>
					    	<td class="text-center"> {{nextRowIndex($statistics->pergunta->id)}} </td>
					    	<td class="text-center"> {{ $statistics->quantidadeResposta }} 	</td>
					    	<td class="text-center"> {{ $statistics->media }}				</td>
	                        <td class="text-center"> {{ $statistics->maximo }}				</td>
	                        <td class="text-center"> {{ $statistics->minimo }}				</td>
	                        <td class="text-center"> {{ $statistics->moda }}				</td>
	                        <td class="text-center"> {{ $statistics->desvioPadrao }}		</td>
	                        <td class="text-center"> {{ $statistics->varianca }}			</td>
			    		</tr>
			    	@endforeach
			    </tbody>
			</table>
			@endforeach
		</div>

	</body>

<?php 

 function nextRowIndex($index) {  
  //$index -=2;
  return $index;
}
?>


<?php function dateFormatBrazil($date) {  
  if ($date == NULL)
    return "";
  
  $data = explode('-', $date);
  $data = substr($data[2], 0, 2).'/'.$data[1].'/'.$data[0];
  return $data;
}
?>

</html>
