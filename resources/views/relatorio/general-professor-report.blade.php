<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style type="text/css">
        	/*span {
        	  	width: 180px !important;
        	  	text-align: left !important;
        	}​*/
        	.text-rotate {
        	     -moz-transform: rotate(90deg);  /* FF3.5+ */
        	       -o-transform: rotate(90deg);  /* Opera 10.5 */
        	  -webkit-transform: rotate(90deg);  /* Saf3.1+, Chrome */
        			  transform: rotate(90deg);
        	}
        	.paragraph {
        		margin-left: 60px;
        	}
        	h4 {
        		font-family: Arial !important;
        	}
        </style>
	</head>
	<body>
		<div class="row">
		    <img src="../public/img/unioeste-logo.png" alt="logo unioeste" style="width:55px;height:40px;float: left">
		    <p class="text-center" style="vertical-align: middle;margin-right: 55px;">UNIVERSIDADE ESTADUAL DO OESTE DO PARANÁ / CAMPUS FOZ DO IGUAÇU<br>CENTRO DE ENGENHARIAS E CIÊNCIAS EXATAS</p>
		</div>

		<br><br>

		<div class="row">
			<h3 class="text-center">Relatório Geral de Avaliação Docente</h3>
		</div>

		<br><br>

		<div class="row">
			<h4 class="paragraph">Informações da Avaliação</h4>
		</div>
		<br>

		<div class="block">
			<span for="evaluation-date"><b>Período da Avaliação:</b></span>
			<span id="evaluation-date">&nbsp;{{dateFormatBrazil($evento->dt_inicio)}} a {{dateFormatBrazil($evento->dt_inicio)}}</span>
		</div>
		<div class="block">
			<span for="course"><b>Curso:</b></span>
			<span id="course">&nbsp;{{$curso->nome}}</span>
		</div>
		<div class="block">
			<span for="description"><b>Descrição:</b></span>
			<span id="description">&nbsp;{{$evento->descricao}}</span>
		</div>

		<br><br>

		<!-- <div class="text-rotate">
		oieeeeee
		</div> -->
		<div class="row">
			<h4 class="paragraph">Questões Abordadas</h4>
		</div>
		<br>
		<div class="row">
			<ol>
			@foreach($perguntas as $key => $value)
				<li> {{$value->nome}} </li>
			@endforeach	
			</ol>
		</div>

		<br><br>

		<div class="row">
			<h4 class="paragraph">Resultado da Avaliação (média por pergunta)</h4>
		</div>
		<br>

		<div class="row">
			<table class="table table-striped">
			    <thead>
			      <tr>
			        <th colspan="1" class="text-center">Disciplina - Professor</th>
			        <th colspan="10" class="text-center">Pergunta</th>
			      </tr>
			      <tr>
			      	<th colspan="1" class="text-center"></th>
 					@for ($i = 1; $i <= count($perguntas); $i++)
					    <th colspan="1" class="text-center">{{$i}}</th>
					@endfor
			      </tr>
			    </thead>
			    <tbody>
		    	@foreach($avaliacaos as $key => $avaliacao)			
			      <tr>
			        <td>{{$avaliacao->disciplina->nome}} - {{$avaliacao->professor->nome}}</td>
			        @foreach($avaliacao->respostaEstatisticas as $key2 => $resposta)
			        	<td class="text-center">{{$resposta->media}}</td>
			        @endforeach
			      </tr>
			      @endforeach	
			    </tbody>
			  </table>
		</div>

	</body>
<?php function dateFormatBrazil($date) {  
  if ($date == NULL)
    return "";
  
  $data = explode('-', $date);
  $data = substr($data[2], 0, 2).'/'.$data[1].'/'.$data[0];
  return $data;
}
?>

</html>