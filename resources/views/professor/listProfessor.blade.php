@extends('layout')

@section('head')

@stop

@section('content')
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
<div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
    <div class="row">
        <h1>Professores</h1>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead class="thead-inverse">
                <tr>
                    <th> ID </th>
                    <th> Professor </th>
                    <th> Ações </th>
                </tr>
            </thead>
            <tbody>
                @foreach($professors as $key => $professor)
                    <tr>
                        <td> {{$professor->id}} </td>
                        <td> {{$professor->nome}} </td>
                        <td width="40%"> 
                            <a id="edit-btn" class="btn btn-default" href="{{'professors/'.$professor->id}}"><i class="fa fa-book " aria-hidden="true"></i> Detalhes</a>
                            <a id="edit-btn" class="btn btn-default" href="#" disabled><i class="fa fa-cog " aria-hidden="true"></i> Editar</a>
                            <a id="edit-btn" class="btn btn-default" href="#" disabled><i class="fa fa-trash " aria-hidden="true"></i> Remover</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
@stop

@section('scripts')
@stop