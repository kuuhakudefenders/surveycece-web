@extends('layout')

@section('head')
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

@stop

@section('content')
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
<div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">
    <div class="row">
        <h1>Avaliacao</h1>
    </div>
    
        <h2>Informações Gerais</h2>
        <p>
            <ul>
                <li><strong>Curso:</strong> {{ $avaliacao->curso->nome }}.</li>
                <li><strong>Disciplina:</strong> {{ $avaliacao->disciplina->nome }}.</li>
                <li><strong>Professor(a):</strong> {{ $avaliacao->professor->nome }}.</li>
                <li><strong>Evento:</strong> {{ $avaliacao->evento->nome }}.</li>
            </ul>
        </p>

        <h2>Perguntas</h2>
        <ol>
            @foreach($questions as $key => $value)
                <li>{{ $value->nome }}.</li>
            @endforeach
        </ol> 

        <h2>Estatisticas</h2>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Pergunta</th>
                    <th>Qntd Respostas</th>
                    <th>Media</th>
                    <th>Mediana</th>
                    <th>Moda</th>
                    <th>Desvio Padrao</th>
                    <th>Variança</th>
                    <th>Maximo</th>
                    <th>Minimo</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach($avaliacao->respostaEstatisticas as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td> {{$value->pergunta->id}}
                        <td>{{ $value->quantidadeResposta }}</td>
                        <td>{{ $value->media }}</td>
                        <td>{{ $value->mediana }}</td>
                        <td>{{ $value->moda }}</td>
                        <td>{{ $value->desvioPadrao }}</td>
                        <td>{{ $value->varianca }}</td>
                        <td>{{ $value->maximo }}</td>
                        <td>{{ $value->minimo }}</td>
                    </tr>
                 @endforeach
            </tbody>
         </table>  

        <h2>Notas</h2>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Pergunta</th>
                    <th>Nota</th>
                    <th>Recebido Em</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach($avaliacao->respostas as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->pergunta->nome }}</td>
                        <td>{{ $value->notaRecebida }}</td>
                        <td>{{ dateFormatBrazil($value->created_at) }}</td>
                    </tr>
                 @endforeach
            </tbody>
         </table>  

<?php function dateFormatBrazil($dateHora) {  
  if ($dateHora == NULL)
    return "";
  
  $data = explode('-', $dateHora);
  $data = substr($data[2], 0, 2).'/'.$data[1].'/'.$data[0];
  // $hora = explode(':', $dateHora);
  // $hora = $hora[0].':'.$hora[1].':'.$hora[2];
  $hora = substr($dateHora, 11, 2).':'.substr($dateHora, 14, 2).':'.substr($dateHora, 17, 2);
  return $data.'  -  '.$hora;
}
?>

@stop

@section('scripts')
@stop