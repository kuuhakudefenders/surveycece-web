@extends('layout')

@section('head')
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@stop

@section('content')
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
<div class="col-lg-6 col-md-8 col-sm-8 col-xs-10">

        <h1>Avaliacao</h1>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Curso</th>
                    <th>Disc</th>
                    <th>Prof</th>
                    <th>Evento</th>
                    
                </tr>
            </thead>
            
            <tbody>
                @foreach($avaliacaos as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->curso->nome }}</td>
                        <td>{{ $value->disciplina->nome }}</td>
                        <td>{{ $value->professor->nome }}</td>
                        <td>{{ $value->evento->nome }}</td>
                        <td>        
                            <a id="edit-btn" class="btn btn-default" href="{{ URL::to('avaliacaos/'. $value->id) }}"><i class="fa fa-book " aria-hidden="true"></i> Detalhes</a>
                            <a id="edit-btn" class="btn btn-default" href="#" disabled><i class="fa fa-cog " aria-hidden="true"></i> Editar</a>
                            <a id="edit-btn" class="btn btn-default" href="#" disabled><i class="fa fa-trash " aria-hidden="true"></i> Remover</a>
                        </td>                    </tr>
                 @endforeach
            </tbody>
        </table>
</div>
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
</div>
@stop

@section('scripts')
@stop