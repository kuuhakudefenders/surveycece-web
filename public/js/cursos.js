$(document).ready(function(event){

	$('#new-curso-save-btn').on('click', function() {
		var courseName = $('#new-curso-name').val();
		
		if (courseName == '') {
			showGenericModal('Nome Inválido', 'Por favor, insira um nome válido.');
		}  else {
			var url = $(this).attr('url');
			var data = {};
			data['nome'] = courseName;
		    $.ajax({
		    	type: "POST",
		      	url: url,
				data: data,
		      	success: function(successData) {
	          		window.location = url;
	            },
		      	error: function() {
		            showGenericModal('Problema', 'Ocorreu um problema para criar o curso. Por favor, contate a equipe técnica.');
				},
		    });
		}
	});

	$('#edit-curso-save-btn').on('click', function() {
		var courseName = $('#edit-name').val();
		var coursesListUrl = $('#cancel-btn').attr('href');
		console.log(coursesListUrl);

		if (courseName == '') {
			showGenericModal('Nome Inválido', 'Por favor, insira um nome válido.');
		}  else {
			var url = $(this).attr('url');
			var data = {};
			data['nome'] = courseName;
		    $.ajax({
		    	type: "PUT",
		      	url: url,
				data: data,
		      	success: function(successData) {
	          		window.location = coursesListUrl;
	            },
		      	error: function(errorData) {
		      		console.log(errorData);
		            showGenericModal('Problema', 'Ocorreu um problema para criar o curso. Por favor, contate a equipe técnica.');
				},
		    });
		};

	});

});