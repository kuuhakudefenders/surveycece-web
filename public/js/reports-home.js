$(document).ready(function(event){
	$('.event-pill').on('click', function() {
		$('.event-pill').parent().removeClass('active');
		$(this).parent().addClass('active');

		$('.report-pill').remove();
		$('.course-pill').remove();
	});

	$('.course-pill').on('click', function() {
		$('.course-pill').parent().removeClass('active');
		$(this).parent().addClass('active');

		$('.report-pill').remove();
	});

});