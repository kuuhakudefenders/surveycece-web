function showGenericModal(title, message) {
	$('#genericModalTitle').text(title);
	$('#genericModalMessage').text(message);
	$('#genericModal').modal('toggle');
};

function showConfirmModal(title, message) {
	$('#confirmModalTitle').text(title);
	$('#confirmModalMessage').text(message);
	$('#confirmModal').modal('toggle');
};

function isInt(value) {
 	return !isNaN(value) && 
    		parseInt(Number(value)) == value && 
    		!isNaN(parseInt(value, 10));
};