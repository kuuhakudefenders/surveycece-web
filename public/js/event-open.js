$(document).ready(function(event){
	
	var openDate = $('#open-date').text();
	var coursesUrl = $('#courses-url').attr('url');
	var formUrl = $('#form-url').attr('url');
	var tokenUrlGen = $('#token-url-gen').attr('url');
	var tokenUrlDow = $('#token-url-dow').attr('url');
	var eventID = $('#event-id').text();
	var currentFormID;

	$.ajax({
	    type: "GET",
	    url: coursesUrl,
	    success: function(successData) {
		    for(var key in successData) {
		    	var opt = document.createElement('option');
		    	opt.value = successData[key]['id'];
		    	opt.innerHTML = successData[key]['nome'];
		    	$('#form-courses').append(opt);
		    }
		}
	});

	$('#edit-btn').on('click', function() {
		openDateSplit = openDate.split("/");
		$('#edit-open-day').val(openDateSplit[0]);
		$('#edit-open-month').val(openDateSplit[1]);
		$('#edit-open-year').val(openDateSplit[2]);
		
		$('#event-show').remove();
		$('#event-edit').removeClass('invisible');
	});

	$('#add-form-btn').on('click', function() {
		$('#create-form-modal').modal('toggle');
	});

	$('#save-form-btn').on('click', function() {
		var courseID = $('#form-courses option:selected').val();
		var data = {};
		data['evento_id'] = eventID;
		data['curso_id'] = courseID;
		console.log(data);
		$.ajax({
		    type: "POST",
		    url: formUrl,
		    data: data,
		    success: function(successData) {
			    location.reload();
			},
			error: function(jqXHR, exception) {
			  	var msg = '';
	        	if (jqXHR.status === 0) {
	            	msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.log(msg);
		        location.reload();
		        // showGenericModal('Error', msg);
			},
		});
	});

	$('#save-btn').on('click', function() {
		var eventName = $('#edit-name').val();
		var eventOpenDay = $('#edit-open-day').val();
		var eventOpenMonth = $('#edit-open-month').val();
		var eventOpenYear = $('#edit-open-year').val();
		var eventDescription = $('#edit-description').val();

		if (eventName == '') {
			showGenericModal('Nome Inválido', 'Por favor, insira um nome válido.');
		} else if (eventOpenDay.match(/[a-z]/i) &&
						eventOpenMonth.match(/[a-z]/i) &&
						eventOpenYear.match(/[a-z]/i)) {
			showGenericModal('Data de Início Inválida', 'Por favor, insira uma data de início válida.');
		} else {
			var url = $(this).attr('url');
			var eventOpenDate = eventOpenYear + '-' + eventOpenMonth + '-' + eventOpenDay;
			var data = {};
			data['nome'] = eventName;
			data['dt_inicio'] = eventOpenDate;
			data['descricao'] = eventDescription;
		    $.ajax({
		    	type: "PUT",
		      	url: url,
				data: data,
		      	success: function(successData) {
	          		location.reload();
	            },
		      	error: function() {
		            location.reload();
				},
		    });
		};

	});

	$('.btn-gen-token').on('click', function() {
		currentFormID = $(this).attr('form-id');
		$('#gen-token-modal').modal('toggle');
	});

	$('#gen-token-btn').on('click', function() {
		var tokenAmount = $('#tokens-num-input').val();

	    var data = {};
	    data['validadoSN'] = 0;
	    data['abertoSN'] = 1;
	    data['tokenGeradoSN'] = 1;
	    
	    $.ajax({
	    	type: "PUT",
	      	url: formUrl + '/' + currentFormID,
	      	data: data,
	      	success: function(successData) {
          		console.log(successData);
          		location.reload();
            },
	      	error: function(jqXHR, exception) {
			  	var msg = '';
	        	if (jqXHR.status === 0) {
	            	msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.log(msg);
		        // showGenericModal('Error', msg);
			},
	    });
	
	    $.ajax({
	    	type: "POST",
	      	url: tokenUrlGen + '/' + currentFormID + '/' + tokenAmount,
	      	data: {},
	      	success: function(successData) {
          		location.reload();
            },
	      	error: function(jqXHR, exception) {
			  	var msg = '';
	        	if (jqXHR.status === 0) {
	            	msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.log(msg);
		        // showGenericModal('Error', msg);
			},
	    });

	});

	$('.btn-validate').on('click', function() {
		currentFormID = $(this).attr('form-id');
		$('#validate-modal').modal('toggle');
	});

	$('#validate-btn').on('click', function() {
		
		var csvResponses = document.getElementById("csvResponses");
		var f = new FormData();
		f.append('file',csvResponses.files[0]);
		var url = csvResponses.getAttribute('data-path') + '/' + currentFormID;
		console.log(url);

		

	    $.ajax({
	    	type: "POST",
	      	url: url,
			data: f,
			cache: false,
	        contentType: false,
	        processData: false,
	        beforeSend: function(){
		        $('#loading-indicator').show();
		    },
		    complete: function(){
		        $('#loading-indicator').hide();
		    },
	      	success: function(successData) {
	      		console.log('Sucess');
	      		var url2 = formUrl + '/' + currentFormID;
	      		console.log(url2);
	      		var data = {};
			    data['validadoSN'] = 1;
			    data['abertoSN'] = 1;
			    data['tokenGeradoSN'] = 1;
	      		$.ajax({
			    	type: "PUT",
			      	url: url2,
			      	data: data,
			      	success: function(successData) {
			      		console.log('2.Sucess');
		          		location.reload();
		            },
			      	error: function(jqXHR, exception) {
					  	var msg = '';
			        	if (jqXHR.status === 0) {
			            	msg = 'Not connect.\n Verify Network.';
				        } else if (jqXHR.status == 404) {
				            msg = 'Requested page not found. [404]';
				        } else if (jqXHR.status == 500) {
				            msg = 'Internal Server Error [500].';
				        } else if (exception === 'parsererror') {
				            msg = 'Requested JSON parse failed.';
				        } else if (exception === 'timeout') {
				            msg = 'Time out error.';
				        } else if (exception === 'abort') {
				            msg = 'Ajax request aborted.';
				        } else {
				            msg = 'Uncaught Error.\n' + jqXHR.responseText;
				        }
				        console.log(msg);
				        location.reload();
				        // showGenericModal('2.Error', msg);
					},
			    });
          		
            },
	      	error: function(jqXHR, exception) {
			  	var msg = '';
	        	if (jqXHR.status === 0) {
	            	msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.log(msg);
		        showGenericModal('Error', msg);
			},
	    });
		

	});

	$('.btn-rm-form').on('click', function() {
		currentFormID = $(this).attr('form-id');
		showConfirmModal('Remover formulário', 'Tem certeza de que você deseja remover este formulário?');
	});

	$('.btn-ok').on('click', function() {
		$.ajax({
	    	type: "DELETE",
	      	url: formUrl + '/' + currentFormID,
	      	success: function(successData) {
          		location.reload();
            },
	      	error: function() {
	      		location.reload();
			},
	    });
	});

	$('.btn-add-link').on('click', function() {
		currentFormID = $(this).attr('form-id');
		$('#add-form-link-modal').modal('toggle');
	});

	$('#add-link-btn').on('click', function() {
		var googleFormLink = $('#link-google-form-input').val();
		var data = {};
		data['linkGoogleForm'] = googleFormLink;

		$.ajax({
	    	type: "POST",
	      	url: formUrl + '/' + currentFormID + '/adicionarLinkFormulario',
	      	data: data,
	      	success: function(successData) {
          		location.reload();
            },
	      	error: function() {
	      		location.reload();
			},
	    });
	});

});