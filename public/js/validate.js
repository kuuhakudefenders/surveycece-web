$(document).ready(function(event){

	$chosenResponseFile = '';
	$chosenTokenFile = '';

	$("input:file").change(function (){
		$chosenResponseFile = $(this).val();

	    canValidate();
    });

	$('a').click(function(){
		$('a').removeClass('active');
		$(this).addClass('active');

		$chosenTokenFile = $(this).text();

		canValidate();
	});

	function canValidate() {
		if ($chosenResponseFile != '' && $chosenTokenFile != '') {
			$('#validateResponsesButton').removeClass('disabled');
		} else {
			$('#validateResponsesButton').addClass('disabled');
		}
	}

	$('#validateResponsesButton').on('click', function(event) {
		var csvResponses = document.getElementById("csvResponses");
		var f = new FormData();
		f.append('file',csvResponses.files[0]);
		var url = csvResponses.getAttribute('data-path') + '/' + $chosenTokenFile;
		console.log(url);
	    $.ajax({
	    	type: "POST",
	      	url: url,
			data: f,
			cache: false,
	        contentType: false,
	        processData: false,
	      	success: function(successData) {
          		console.log('sucesso!');
            },
	      	error: function() {
	            console.log('falha :(');
			},
	    });
	});


});
