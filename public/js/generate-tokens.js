$(document).ready(function(event){

	$('#generateTokensBtn').on('click', function(event) {
		var tokenAmount = document.getElementById("tokensNumInput").value;

		if (!isInt(tokenAmount)) {
			showGenericModal('Valor de senha inválido', 'Por favor, insira somente números inteiros.');
		} else if (tokenAmount > 10000) {
			showGenericModal('Valor de senha inválido', 'Por favor, insira um valor menor que 10.000.');
		} else if (tokenAmount <= 0) {
			showGenericModal('Valor de senha inválido', 'Por favor, insira um valor positivo.');
		} else {
			var element = document.getElementById("tokensNumInput");
			var url = element.getAttribute('data-path') + '/' + tokenAmount;
			console.log(url);
		    $.ajax({
		    	type: "POST",
		      	url: url,
		      	success: function(successData) {
	          		console.log('sucesso!');
	            },
		      	error: function() {
		            console.log('falha :(');
				},
		    });
		}
	});

});
