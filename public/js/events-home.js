$(document).ready(function(event){

	var currentEventUrl = '';
	var currentEventName = '';
	var currentEventDate = '';

	$('.card-add').hover(function(){
	    $('#add-event-icon').css("color", "#7DEC9E");
	    }, function() {
	    $('#add-event-icon').css("color", "#DDD");
	});

	$('.card-open').hover(function() {
		$(this).find('button').each(function() {
			$(this).removeClass('invisible');
		});
		}, function() {
		$(this).find('button').each(function() {
			$(this).addClass('invisible');
		});
	});

	$('.card-closed').hover(function() {
		$(this).find('button').each(function() {
			$(this).removeClass('invisible');
		});
		}, function() {
		$(this).find('button').each(function() {
			$(this).addClass('invisible');
		});
	});

	$('#new-event-save-btn').on('click', function() {
		var eventName = $('#new-event-name').val();
		var eventDescription = $('#new-event-descricao').val();
		var eventDay = $('#new-event-day').val();
		var eventMonth = $('#new-event-month').val();
		var eventYear = $('#new-event-year').val();
		
		if (eventName == '') {
			showGenericModal('Nome Inválido', 'Por favor, insira um nome válido.');
		} else if (eventDay.match(/[a-z]/i) &&
						eventMonth.match(/[a-z]/i) &&
						eventYear.match(/[a-z]/i)) {
			showGenericModal('Data Inválido', 'Por favor, insira uma data válida.');
		} else {
			var url = $(this).attr('url');
			var eventDate = eventYear + '-' + eventMonth + '-' + eventDay;
			var data = {};
			data['nome'] = eventName;
			data['descricao'] = eventDescription;
			data['dt_inicio'] = eventDate;
		    $.ajax({
		    	type: "POST",
		      	url: url,
				data: data,
		      	success: function(successData) {
	          		window.location = url;
	            },
		      	error: function() {
		            showGenericModal('Problema', 'Ocorreu um problema para criar o evento. Por favor, contate a equipe técnica.');
				},
		    });
		}
	});


	var deleteEventUrl = '';

	$('.btn-rm').on('click', function() {
		showConfirmModal('Remover Evento', 'Você tem certeza que deseja remover este evento?');
		deleteEventUrl = $(this).attr('url');
	});

	$('.btn-ok').on('click', function() {
	    $.ajax({
	    	type: "DELETE",
	      	url: deleteEventUrl,
	      	success: function(successData) {
          		location.reload();
            },
	      	error: function() {
	            location.reload();
			},
	    });
	});

	$('.btn-close-event').on('click', function() {
		currentEventUrl = $(this).attr('url');
		$('#close-event-modal').modal('toggle');
	});

	$('#confirm-close-event').on('click', function() {
		$.ajax({
	    	type: "GET",
	      	url: currentEventUrl,
	      	success: function(successData) {
          		location.reload();
            },
	      	error: function() {
	            
			},
	    });
	});

});