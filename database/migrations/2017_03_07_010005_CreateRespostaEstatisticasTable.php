<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespostaEstatisticasTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('respostasEstatisticas', function (Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->double('media')->nullable(false);  
            $table->double('moda')->nullable(false);
            $table->double('desvioPadrao')->nullable(false);
            $table->double('mediana')->nullable(false);    
            $table->double('maximo')->nullable(false);  
            $table->double('minimo')->nullable(false);
            $table->double('varianca')->nullable(false);
            $table->integer('quantidadeResposta')->nullable(false);
            
            $table->integer('avaliacao_id')->unsigned()->nullable(false);
            $table->integer('pergunta_id')->unsigned()->nullable(false);
            
            $table->unique(array('avaliacao_id', 'pergunta_id'));

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('respostasEstatisticas', function($table) {
            $table->foreign('avaliacao_id')->references('id')->on('avaliacao')->onDelete('cascade');
            $table->foreign('pergunta_id')->references('id')->on('pergunta')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('respostasEstatisticas');
    }
}