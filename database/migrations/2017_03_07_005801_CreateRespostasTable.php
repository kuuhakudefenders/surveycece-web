<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespostasTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('resposta', function (Blueprint $table){
            $table->bigIncrements('id')->nullable(false);
            $table->integer('notaRecebida')->unsigned()->nullable(false);        
            
            $table->integer('avaliacao_id')->unsigned()->nullable(false);
            $table->integer('pergunta_id')->unsigned()->nullable(false);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('resposta', function($table) {
            $table->foreign('avaliacao_id')->references('id')->on('avaliacao')->onDelete('cascade');
            $table->foreign('pergunta_id')->references('id')->on('pergunta')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('resposta');
    }
}