<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokensTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('token', function (Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->string('valor',5)->nullable(false);
            $table->integer('formulario_id')->unsigned()->nullable(false);
            $table->unique(array('formulario_id', 'valor'));

            $table->timestamps();
        });

        Schema::table('token', function($table) {
            $table->foreign('formulario_id')->references('id')->on('formulario')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('token');
    }

}
