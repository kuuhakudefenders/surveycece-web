<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplinasTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('disciplina', function (Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->string('nome',50)->nullable(false);
            $table->integer('professor_id')->unsigned()->nullable(false);
            $table->integer('curso_id')->unsigned()->nullable(false);
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('disciplina', function($table) {
            $table->foreign('professor_id')->references('id')->on('professor')->onDelete('cascade');
            $table->foreign('curso_id')->references('id')->on('curso');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('disciplina');
    }
}
