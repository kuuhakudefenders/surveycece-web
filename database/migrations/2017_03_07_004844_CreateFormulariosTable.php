<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulariosTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('formulario', function (Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->boolean('validadoSN')->default(false)->nullable(false);
            $table->boolean('abertoSN')->default(true)->nullable(false);
            $table->boolean('tokenGeradoSN')->default(false)->nullable(false);
            $table->string('linkGoogleForm')->nullable(true);

            $table->integer('evento_id')->unsigned()->nullable(false);
            $table->integer('curso_id')->unsigned()->nullable(false);
            $table->unique(array('curso_id', 'evento_id'));

            $table->timestamps();
        });

        Schema::table('formulario', function($table) {
            $table->foreign('evento_id')->references('id')->on('evento')->onDelete('cascade');
            $table->foreign('curso_id')->references('id')->on('curso');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('formulario');
    }
}
