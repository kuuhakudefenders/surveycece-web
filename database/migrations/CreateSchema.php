<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchema extends Migration{
	public function up(){
		// BD:statement('CREATE SCHEMA surveyCECE');
		DB::getConnection()->statement('CREATE DATABASE :schema', array('schema' => 'surveyCECE'));
	}

	public function down(){
		// BD:statement('DROP SCHEMA surveyCECE');
		DB::getConnection()->statement('DROP DATABASE :schema', array('schema' => 'surveyCECE'));
	}
}
>