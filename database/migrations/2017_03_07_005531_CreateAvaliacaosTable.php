<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliacaosTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('avaliacao', function (Blueprint $table){
            $table->increments('id')->nullable(false);
            
            $table->integer('curso_id')->unsigned()->nullable(false);
            $table->integer('professor_id')->unsigned()->nullable(false);
            $table->integer('evento_id')->unsigned()->nullable(false);
            $table->integer('disciplina_id')->unsigned()->nullable(false);
            
            $table->unique(array('curso_id', 'evento_id', 'professor_id','disciplina_id'));

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('avaliacao', function($table) {
            $table->foreign('evento_id')->references('id')->on('evento')->onDelete('cascade');
            $table->foreign('professor_id')->references('id')->on('professor')->onDelete('cascade');
            $table->foreign('disciplina_id')->references('id')->on('disciplina')->onDelete('cascade');
            $table->foreign('curso_id')->references('id')->on('curso')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('avaliacao');
    }
}
