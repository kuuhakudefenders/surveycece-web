<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('evento', function (Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->string('descricao')->nullable(true);
            $table->date('dt_inicio')->nullable(false);
            $table->date('dt_fim')->nullable(true);
            $table->boolean('abertoSN')->default(true)->nullable(false);
            $table->string('nome',100)->nullable(false);
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('evento');
    }
}
