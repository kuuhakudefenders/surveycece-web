<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	DB::table('users')->insert([
            'name' => 'Coordenação Unioeste CECE Foz',
            'email' => 'coordareacecefoz@gmail.com',
            'password' => bcrypt('cecefoz0605'),
        ]);
    }
}

