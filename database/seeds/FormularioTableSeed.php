<?php

use Illuminate\Database\Seeder;

class FormularioTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('formulario')->insert([
            'curso_id' => '1',
            'evento_id' => '1',
            'linkGoogleForm' => 'https://s-media-cache-ak0.pinimg.com/originals/18/3b/85/183b858b0a4e8de0d7de6d3a3e626279.png'
        ]);

        DB::table('formulario')->insert([
            'curso_id' => '2',
            'evento_id' => '1',
            'linkGoogleForm' => 'http://2.bp.blogspot.com/-B69Hx06Lb-s/T-SSoTqcnxI/AAAAAAAABgI/VXwVS0HcUJw/s1600/Meme5.jpg'
        ]);

        DB::table('formulario')->insert([
            'curso_id' => '3',
            'evento_id' => '1',
            'linkGoogleForm' => 'http://ragefaces.memesoftware.com/faces/svg/misc-herp-derp.svg'
        ]);

        DB::table('formulario')->insert([
            'curso_id' => '4',
            'evento_id' => '1',
            'linkGoogleForm' => 'http://i1276.photobucket.com/albums/y474/Phoenix_B_Lupina/276251_Papel-de-Parede-Meme-Are-You-Kidding-Me_1152x864_zps66b551c1.jpg'
        ]);


        DB::table('formulario')->insert([
            'curso_id' => '1',
            'evento_id' => '2',
            'validadoSN' => '1',
            'abertoSN' => '0',
            'tokenGeradoSN' => '1',
            'linkGoogleForm' => 'http://www.walldevil.com/wallpapers/w03/881304-meme-surprise.jpg'
        ]);

        DB::table('formulario')->insert([
            'curso_id' => '2',
            'evento_id' => '2',
            'validadoSN' => '1',
            'abertoSN' => '0',
            'tokenGeradoSN' => '1',
            'linkGoogleForm' => 'https://imgflip.com/readImage?iid=8064765'
        ]);

        DB::table('formulario')->insert([
            'curso_id' => '3',
            'evento_id' => '2',
            'validadoSN' => '1',
            'abertoSN' => '0',
            'tokenGeradoSN' => '1',
            'linkGoogleForm' => 'https://docscastle.files.wordpress.com/2014/10/imgres2.jpg'
        ]);

        DB::table('formulario')->insert([
            'curso_id' => '4',
            'evento_id' => '2',
            'validadoSN' => '1',
            'abertoSN' => '0',
            'tokenGeradoSN' => '1',
            'linkGoogleForm' => 'https://s-media-cache-ak0.pinimg.com/736x/91/92/e2/9192e2fe297285a2407dc441bbdb5e87.jpg'
        ]);
    }
}
               
