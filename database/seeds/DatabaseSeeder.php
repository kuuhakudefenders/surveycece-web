<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(CursoTableSeed::class);
        // $this->call(EventoTableSeed::class);
        // $this->call(FormularioTableSeed::class);
        $this->call(UsersTableSeeder::class);
        // $this->call(ProfessorTableSeed::class);
        // $this->call(DisciplinaTableSeed::class);
    }
}
