<?php

use Illuminate\Database\Seeder;

class CursoTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('curso')->insert(['nome' => 'Ciência da Computação']);
        DB::table('curso')->insert(['nome' => 'Engenharia Elétrica']);
        DB::table('curso')->insert(['nome' => 'Engenharia Mecânica']);
        DB::table('curso')->insert(['nome' => 'Matemática']);
    }
}
