<?php

use Illuminate\Database\Seeder;

class ProfessorTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('professor')->insert([
            'nome' => 'João',
        ]);

        DB::table('professor')->insert([
            'nome' => 'José da Silva',
        ]);

        DB::table('professor')->insert([
            'nome' => 'Joana Silveira',
        ]);

        DB::table('professor')->insert([
            'nome' => 'Amanda Martelo',
        ]);

    }
}
