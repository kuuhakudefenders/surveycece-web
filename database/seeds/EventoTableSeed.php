<?php

use Illuminate\Database\Seeder;

class EventoTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('evento')->insert([
            'nome' => 'CECE 2016/06',
            'dt_inicio' => '2016-06-01'
        ]);

        DB::table('evento')->insert([
            'id' => '2',
            'nome' => 'CECE 2015/12',
            'dt_inicio' => '2015-12-01',
            'dt_fim' => '2015-12-20',
            'abertoSN' => '0',
            'descricao' => 'Avaliacao dos professores referente ao segundo semestre de 2015'
        ]);

        DB::table('evento')->insert([
            'id' => '3',
            'nome' => 'CECE 2016/12',
            'dt_inicio' => '2016-12-01'
        ]);

        DB::table('evento')->insert([
            'id' => '4',
            'nome' => 'CECE 2015/06',
            'dt_inicio' => '2015-06-01',
            'dt_fim' => '2015-06-29',
            'abertoSN' => '0',
        ]);
    }
}
