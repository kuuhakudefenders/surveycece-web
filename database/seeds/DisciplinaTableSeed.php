<?php

use Illuminate\Database\Seeder;

class DisciplinaTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('disciplina')->insert([
            'nome' => 'Geometria Analítica',
            'professor_id' => '1',
            'curso_id' => '1',
        ]);
        DB::table('disciplina')->insert([
            'nome' => 'Computação 1',
            'professor_id' => '2',
            'curso_id' => '1',
        ]);
        DB::table('disciplina')->insert([
            'nome' => 'Cálculo',
            'professor_id' => '2',
            'curso_id' => '1',
        ]);
        DB::table('disciplina')->insert([
            'nome' => 'Física',
            'professor_id' => '3',
            'curso_id' => '1',
        ]);
        DB::table('disciplina')->insert([
            'nome' => 'Redes de Computadores',
            'professor_id' => '4',
            'curso_id' => '1',
        ]);
    }
}
