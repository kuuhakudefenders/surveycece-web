<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curso extends Model{
    protected $table = 'curso'; 
    public $timestamps = true;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function disciplinas(){
        return $this->hasMany('App\Model\Disciplina','curso_id')->orderBy('nome');
    }

    public function avaliacaos(){
        return $this->hasMany('App\Model\Avaliacao');
    }
}
