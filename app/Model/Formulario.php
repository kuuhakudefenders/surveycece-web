<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formulario extends Model{
    protected $table = 'formulario'; 
    public $timestamps = true;
    
    public function tokens(){
        return $this->hasMany('App\Model\Token','id');
    }

    public function curso(){
        return $this->belongsTo('App\Model\Curso','id');
    }

    public function evento(){
        return $this->belongsTo('App\Model\Evento');
    }
}
