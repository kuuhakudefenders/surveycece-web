<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Professor extends Model{
    protected $table = 'professor'; 
    public $timestamps = true;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function disciplinas(){
        return $this->hasMany('App\Model\Disciplina');
    }

    public function avaliacaos(){
        return $this->hasMany('App\Model\Avaliacao');
    }
}
