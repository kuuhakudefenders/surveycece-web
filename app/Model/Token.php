<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Token extends Model{
    protected $table = 'token'; 
    public $timestamps = true;

    public function formulario(){
        return $this->belongsTo('App\Model\Formulario');
    }
}
