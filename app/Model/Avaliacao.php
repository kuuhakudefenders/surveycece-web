<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Avaliacao extends Model{
    protected $table = 'avaliacao'; 
    public $timestamps = true;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function professor(){
        return $this->belongsTo('App\Model\Professor');
    }

    public function curso(){
        return $this->belongsTo('App\Model\Curso');
    }

    public function disciplina(){
        return $this->belongsTo('App\Model\Disciplina');
    }

    public function evento(){
        return $this->belongsTo('App\Model\Evento');
    }

	public function respostaEstatisticas(){
        return $this->hasMany('App\Model\RespostaEstatistica','avaliacao_id');
    } 

    public function respostas(){
        return $this->hasMany('App\Model\Resposta');
    }   
}
