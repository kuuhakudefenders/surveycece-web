<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evento extends Model{
    protected $table = 'evento'; 
    public $timestamps = true;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

     public function formularios(){
        return $this->hasMany('App\Model\Formulario','evento_id');
    }

    public function avaliacaos(){
        return $this->hasMany('App\Model\Avaliacao');
    }
}
