<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Disciplina extends Model{
    protected $table = 'disciplina'; 
    public $timestamps = true;

    use SoftDeletes;

    public function professor(){
        return $this->belongsTo('App\Model\Professor');
    }

    public function curso(){
        return $this->belongsTo('App\Model\Curso');
    }

    public function avaliacaos(){
        return $this->hasMany('App\Model\Avaliacao');
    }
}
