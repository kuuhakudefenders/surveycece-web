<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resposta extends Model{
    protected $table = 'resposta'; 
    public $timestamps = true;

    public function avaliacao(){
        return $this->belongsTo('App\Model\Avaliacao');
    }

    public function pergunta(){
        return $this->belongsTo('App\Model\Pergunta','pergunta_id');
    }
}