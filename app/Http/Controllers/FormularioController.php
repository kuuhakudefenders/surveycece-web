<?php

namespace App\Http\Controllers;

use App\Model\Formulario;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Response;

class FormularioController extends Controller{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        $formulario = Formulario::find($id);

        return View::make('formulario.showFormulario')->with('formulario', $formulario);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $formularios =  Formulario::all();
        
        return View::make('formulario.listFormulario')->with('formularios', $formularios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
         return View::make('formulario.createFormulario');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(){
        $rules = array(
            'curso_id'       => 'required',
            'evento_id'       => 'required');
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::to('formularios/create')->withErrors($validator);
        } else {
            $formulario = new Formulario;
            $formulario->curso_id = Input::get('curso_id');
            $formulario->evento_id = Input::get('evento_id');
            $formulario->save();

            //return $formulario;
            return Redirect::to('formularios');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        $formulario = Formulario::find($id);
        if($formulario->abertoSN == 1 ){
            return View::make('formulario.editFormulario')->with('formulario', $formulario);
        } else{
            return Redirect::to('formularios');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id){
        $rules = array(
            'validadoSN'       => 'required',
            'abertoSN'       => 'required',
            'tokenGeradoSN'       => 'required');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('formularios/' . $id . '/edit')->withErrors($validator);
        } else {
            $formulario = Formulario::find($id);
            $formulario->validadoSN = Input::get('validadoSN');
            $formulario->abertoSN = Input::get('abertoSN');
            $formulario->tokenGeradoSN = Input::get('tokenGeradoSN');
            $formulario->save();

            Session::flash('message', 'Formulario atualizado com sucesso!');
            return Redirect::to('formularios');
        }
    }

    public function addLinkGoogleForm($id){
        $rules = array(
            'linkGoogleForm'       => 'required');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('formularios/' . $id . '/edit')->withErrors($validator);
        } else {
            $formulario = Formulario::find($id);
            $formulario->linkGoogleForm = Input::get('linkGoogleForm');
            $formulario->save();

            Session::flash('message', 'Link do formulário inserido com sucesso!');
            return Redirect::to('formularios');
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        $formulario = Formulario::find($id);
        $formulario->delete();

        Session::flash('message', 'Formulario removido com sucesso!');
        return Redirect::to('formularios');
    }

}