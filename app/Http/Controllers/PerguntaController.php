<?php

namespace App\Http\Controllers;

use App\Model\Pergunta;
use App\Http\Controllers\Controller;
use View;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Response;

class PerguntaController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $perguntas =  Pergunta::all();
        return View::make('pergunta.listPergunta')->with('perguntas', $perguntas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeOne($pergunta){ // aceito sugestoes de nome :<

        if ($pergunta instanceof Pergunta 
                && !empty($pergunta->nome)){

            $result = DB::table('pergunta')->where('nome', $pergunta->nome);
            if($result->first()){
                $pergunta->id = $result->first()->id;
            } else{
                $pergunta->save();
            }
        }
        return $pergunta;
    }
    
}