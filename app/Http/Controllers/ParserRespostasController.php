<?php

namespace App\Http\Controllers;

use File;
use View;
use App;
use Storage;
use App\User;
use App\Model\Curso;
use App\Model\Professor;
use App\Model\Disciplina;
use App\Model\Token;
use App\Model\Evento;
use App\Model\Avaliacao;
use App\Model\Formulario;
use App\Model\Pergunta;
use App\Model\Resposta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ParserRespostasController extends Controller {

    private function insertProfessorAndDiscipline($curso_id, $pieces){
        $arrayProfDiscItem = array();
        if ( count($pieces) == 2 && strlen($pieces[0])  && strlen($pieces[1]) ){
            $professor = new Professor();
            $professor->nome = $pieces[1];
            
            $professor = app('App\Http\Controllers\ProfessorController')->storeOne($professor);
            $curso = app('App\Http\Controllers\CursoController')->showOne($curso_id);
            $disciplina =  new Disciplina();
            $disciplina->nome = $pieces[0];
            $disciplina->professor_id = $professor->id;
            $disciplina->curso_id = $curso->id;
            $disciplina = app('App\Http\Controllers\DisciplinaController')->storeOne($disciplina);

            $arrayProfDiscItem[0] = $disciplina;
            $arrayProfDiscItem[1] = $professor;
        }
        return $arrayProfDiscItem;
    }

    private function parserProfessorsAndDisciplines($curso_id, $fileContent){
        $arrayProfessors = array();
        $arrayDisciplines = array();
        
        // Separa apenas a primeira linha
        $endLinePosition = strpos($fileContent, "\n");
        $fileContent = substr($fileContent, 0, $endLinePosition-1);
        $fileContent .= ",";
        $arrayProfDisc = array();
        while ( strpos($fileContent, ",") !== false ){
            $newEnd = strpos($fileContent, ",");
            while (strlen($fileContent) > 1 && !$newEnd){
                $fileContent = substr($fileContent, $newEnd+1);
                $newEnd = strpos($fileContent, ",");
            }

            $line = substr($fileContent,0,$newEnd);
            $pieces = explode("-",$line);

            $pieces = $this->insertProfessorAndDiscipline($curso_id,$pieces);
            if ( count($pieces) == 2) {
                $arrayProfDisc[] = $pieces;
            }

            $fileContent = substr($fileContent, $newEnd+1);
        }

        return $arrayProfDisc;
    }

    private function parserPergunta($fileContent)
    {
        $newEnd = strpos($fileContent, "\n");
        $fileContent = substr($fileContent, $newEnd+1);

        $newEnd = strpos($fileContent, "\n");
        $secondLine = substr($fileContent, 0, $newEnd);

        $separator = "____________";
        $secondLine = str_replace(",(", $separator."(" ,$secondLine); 
        $secondLine = str_replace(",\"", $separator."\"" ,$secondLine); 
        $secondLineColumns = explode($separator, $secondLine);

        $arrayPergunta = array();
        foreach ($secondLineColumns as $key => $value){
            $pos = strpos($value,")");
            if($pos && is_numeric($value[$pos-1])){
                $value = substr($value, $pos+1);
                if($value[0] == " "){
                    $value = substr($value, 1);
                }
                $pos = strpos($value,"(");
                if($pos){
                    $value = substr($value, 0, $pos);
                }
                $pos = strlen($value) -1;
                if($value[$pos] == " "){
                    $value = substr($value, 0, $pos);
                }
                $secondLineColumns[$key] = $value;

                $pergunta = new Pergunta;
                $pergunta->nome = $value;
                $pergunta = app('App\Http\Controllers\PerguntaController')->storeOne($pergunta);
                $arrayPergunta[$key] = $pergunta;
            } else{
                unset($secondLineColumns[$key]);
            }
        }

        return $arrayPergunta;
    }

    private function parserResposta($evento_id, $curso_id, $fileContent, $arrayPergunta, $arrayProfDisc){
        // Grab first line
        $newEnd = strpos($fileContent, "\n");
        $firstLine = substr($fileContent, 0, $newEnd);

        // Grab answers lines
        $fileContent = substr($fileContent, $newEnd+1);
        $newEnd = strpos($fileContent, "\n");
        $answers = substr($fileContent, $newEnd+1);

        // Get [columns number] => discipline (model)
        // at $firstLineColumns
        $firstLineColumns = explode(",", $firstLine);
        foreach ($firstLineColumns as $key => $value){
            // If we have a column "discpline - professor"
            // then grab that column number: "$key"
            $pos = strpos($value,"-");
            if($pos){
                $disciplina = substr($value, 0, $pos);
                foreach ($arrayProfDisc as $key2 => $value2)
                {
                    if($value2[0]->nome == $disciplina){
                        $firstLineColumns[$key] = $value2;
                    }
                }
            } else{
                unset($firstLineColumns[$key]);
            }
        }
        // Insert one last item so we know where the last discipline questions end
        // => so we catch the last discipline question and iterate over questions line
        // when we find something with lenght>0 (ex: Observações) we stop
        $firstLine = explode(",", $firstLine);
        $disciplines = array_keys($firstLineColumns);
        for($i=$disciplines[count($disciplines)-1]+1; $i<count($firstLine); $i++){
            if( strlen($firstLine[$i])>0 )
                break;
        }
        $firstLineColumns[] = $i;

        $answers = explode("\n",$answers);
        $avaliacaos = array();

        // for each answer make one Avaliacao
        foreach ($answers as $key => $value)
        {
            $answer = explode(",",$value);

            $disciplines = array_keys($firstLineColumns);

            // for each discipline column make one Avaliacao
            // remember: the last one is just a mark, not an actual question
            
            for( $i=0; $i<count($disciplines)-1; $i++){
                $sum = 0;
                $disciplineColumnStart = $disciplines[$i];
                $disciplineColumnEnd = $disciplines[$i+1];
                $discipline_id = $firstLineColumns[$disciplines[$i]][0]->id;
                
                $respostas = array();
                // for each column make one resposta
                for( $j=$disciplineColumnStart; $j<$disciplineColumnEnd; $j++){
                    if( array_key_exists($j, $answer)){
                        $nota = intval( $answer[$j] );
                        if( $nota >= 0 && $nota<=10 ){
                            // if there is no more questions, stop
                            if( !in_array($j,array_keys($arrayPergunta)) )
                                break;
                            $resposta = new Resposta();
                            $resposta->notaRecebida = $nota;
                            $resposta->pergunta_id = $arrayPergunta[$j]->id;
                            $sum += $nota;
                            array_push($respostas, $resposta);
                        }
                    }
                }
                if ($sum!= 0) {                        
                    if( array_key_exists($discipline_id, $avaliacaos)){
                        $avaliacao = $avaliacaos[$discipline_id];
                    } else{
                        $avaliacao = new Avaliacao();
                        $avaliacao->curso_id = $curso_id;
                        $avaliacao->professor_id = $firstLineColumns[$disciplines[$i]][1]->id;
                        $avaliacao->evento_id = $evento_id;
                        $avaliacao->disciplina_id = $discipline_id;
                        $avaliacao = app('App\Http\Controllers\AvaliacaoController')->storeOne($avaliacao);
                        $avaliacaos[$discipline_id] = $avaliacao;
                    }
                    
                    foreach ($respostas as $key => $resposta) {
                        $resposta->avaliacao_id = $avaliacao->id;
                        app('App\Http\Controllers\RespostaController')->storeOne($resposta);
                    }
                }
            }
        }

        return $firstLineColumns;
        /*
            IR PASSANDO EM ANSWERS E ARMAZENAR A PONTUACAO DADA NA CELULA X, PARA ISSO É NECESSÁRIO O ID
            DOS PROF., DISC., E PA, OS QUAIS ESTAO ARMAZENADOS EM ARRAYTIPOITEM E FIRSTLINECOLLUMS, COM
            A MESMA KEY DO ARQUIVO ORIGINAL, PRINTE OS DOIS RETURN PARA AVERIGUAR COM MAIS DETALHES
        */
    }

    public function parserFile($evento_id, $curso_id, $fileContent){
        $arrayProfDisc = $this->parserProfessorsAndDisciplines($curso_id, $fileContent);
        $arrayPergunta = $this->parserPergunta($fileContent);
        $this->parserResposta($evento_id, $curso_id, $fileContent, $arrayPergunta, $arrayProfDisc);
        app('App\Http\Controllers\AvaliacaoController')->calculateStatisticsForForm($evento_id,$curso_id);
    }
}
