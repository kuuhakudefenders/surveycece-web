<?php

namespace App\Http\Controllers;

use App\Model\Avaliacao;
use App\Model\Pergunta;
use App\Model\RespostaEstatistica;
use App\Http\Controllers\Controller;
use View;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Response;

class AvaliacaoController extends Controller{
    
    private function calculateStatistics($id){
        $avaliacao = Avaliacao::find($id);
        $statistics = array();
        $respostas = $avaliacao->respostas;
        $questions = $this->getQuestions($respostas);
        $notas = array();
        
        foreach ($questions as $key => $value) {
            $statistics[$key] = array_fill(0,8,0); 
            /*
                statistics[i][j]
                    i = ID da pergunta
                    j = estatistica avaliada
                        0 = quantidade de resposta
                        1 = media
                        2 = mediana
                        3 = desvio padrao
                        4 = moda
                        5 = maximo
                        6 = minimo
                        7 = varianca
            */
            $notas[$key] = array();
        }

        foreach ($respostas as $value) {
            $key = $value->pergunta()->first()->id;
            $statistics[$key][0] += 1.0;
            $statistics[$key][1] += $value->notaRecebida;
            array_push($notas[$key], $value->notaRecebida);
        }

        foreach ($statistics as $key => $value) {
            //media
            $statistics[$key][1] = $statistics[$key][1] / $statistics[$key][0];
        }

        foreach ($notas as $key => $notasFromQuestion) {
            sort($notasFromQuestion);
            // valor máximo
            $statistics[$key][5] = end($notasFromQuestion);

            // valor mínimo
            $statistics[$key][6] = $notasFromQuestion[0];

            // mediana
            $size = count($notasFromQuestion);
            if ($size % 2 == 0) {
               $statistics[$key][2] = $notasFromQuestion[$size/2] + $notasFromQuestion[$size/2 - 1]; 
            } else{
                $statistics[$key][2] = $notasFromQuestion[$size/2];
            }

            foreach ($notasFromQuestion as $nota) {
                $statistics[$key][7] += pow($nota - $statistics[$key][1],2);
            }

            $count=array_count_values($notasFromQuestion);  //Counts the values in the array, returns associatve array
            arsort($count);                                 //Sort it from highest to lowest
            $result=array_keys($count);                     //Split the array so we can find the most occuring key
            $statistics[$key][4] = $result[0];
        }

        foreach ($statistics as $key => $value) {
            if($statistics[$key][0] <= 1){
                $statistics[$key][7] = 0;
                $statistics[$key][3] = 0;
            } else{
                //variança
                $statistics[$key][7] = $statistics[$key][7] / ($statistics[$key][0] - 1);
                //desvio apdrao
                $statistics[$key][3] = sqrt($statistics[$key][7]);   
            }
            
        }

        foreach ($statistics as $key => $value) {
            $respostaEstatistica = new RespostaEstatistica;
            $respostaEstatistica->media = round($statistics[$key][1],2);
            $respostaEstatistica->mediana = round($statistics[$key][2],2);
            $respostaEstatistica->desvioPadrao = round($statistics[$key][3],2);
            $respostaEstatistica->moda = $statistics[$key][4];
            $respostaEstatistica->maximo = round($statistics[$key][5],2);
            $respostaEstatistica->minimo = round($statistics[$key][6],2);
            $respostaEstatistica->varianca = round($statistics[$key][7],2);
            $respostaEstatistica->quantidadeResposta = $statistics[$key][0];
            $respostaEstatistica->avaliacao_id = $id;
            $respostaEstatistica->pergunta_id = $key;
            $respostaEstatistica->save();
        }
     
    }

    private function getBasicRelations($avaliacao){
        $avaliacao->professor =  $avaliacao->professor()->first();
        $avaliacao->curso =  $avaliacao->curso()->first();
        $avaliacao->disciplina =  $avaliacao->disciplina()->first();
        $avaliacao->evento =  $avaliacao->evento()->first();

        return $avaliacao;
    }

    private function getAllRelations($avaliacao){
        $avaliacao = $this->getBasicRelations($avaliacao);
        $avaliacao->resposta =  $avaliacao->respostas;
        $avaliacao->respostaEstatistica =  $avaliacao->respostaEstatisticas;
    
        $respostas = $avaliacao->resposta;
        foreach ($respostas as $key => $value) {

            $value->pergunta = $value->pergunta()->first();
        }
        $avaliacao->resposta = $respostas;

        $respostaEstatisticas = $avaliacao->respostaEstatistica;
        foreach ($respostaEstatisticas as $key => $value) {

            $value->pergunta = $value->pergunta()->first();
        }
        $avaliacao->respostaEstatistica = $respostaEstatisticas;

        return $avaliacao;
    }

    private function getReportRelations($avaliacao){
        $avaliacao = $this->getBasicRelations($avaliacao);
        $avaliacao->respostaEstatistica = DB::table('respostasEstatisticas')
                ->where('avaliacao_id', $avaliacao->id)
                ->get(array('id','media','pergunta_id','avaliacao_id'));
        
        $respostaEstatisticas = $avaliacao->respostaEstatistica;
        foreach ($respostaEstatisticas as $key => $value) {
            $value->pergunta = DB::table('pergunta')
                ->where('id', $value->pergunta_id)
                ->get()->first();
        }
        $avaliacao->respostaEstatistica = $respostaEstatisticas;
    
        return $avaliacao;
    }

    public function getQuestions($respostas){
        $questions = array();
        foreach ($respostas as $key => $value) {
            $question = $value->pergunta()->first();
            $questions[$question->id] = $question;
        }
        return $questions;
    }

    public function getAvaliacaoFromEvento($evento_id){
        $avaliacaos = Avaliacao::where('evento_id',$evento_id)->get();
        foreach ($avaliacaos as $key => $avaliacao) {
            $avaliacao = $this->getReportRelations($avaliacao);
            if($avaliacao == NULL){
                unset($avaliacaos[$key]);
            }
        }
        
        return $avaliacaos;
    }

    public function getAvaliacaoEspecifica($evento_id, $curso_id, $professor_id){
        $avaliacaos = Avaliacao::where('evento_id','=', $evento_id)
                ->where('curso_id','=',$curso_id)
                ->where('professor_id','=',$professor_id)
                ->get();
        foreach ($avaliacaos as $key => $value) {
            $value = $this->getBasicRelations($value);
            $value->respostaEstatisticas;
        }
        
        return $avaliacaos;
    }

    public function calculateStatisticsForForm($evento_id, $curso_id){
        $avaliacaos = Avaliacao::where('evento_id','=', $evento_id)
                ->where('curso_id','=',$curso_id)
                ->get();
        foreach ($avaliacaos as $key => $value) {
            $this->calculateStatistics($value->id);        
        }
        
    }


    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        // $this->calculateStatistics($id);
        $avaliacao = Avaliacao::find($id);
        $avaliacao = $this->getAllRelations($avaliacao);
        $questions = $this->getQuestions($avaliacao->respostas);

        return View::make('avaliacao.showAvaliacao', compact('avaliacao', 'questions'));
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $avaliacaos =  Avaliacao::all();
        foreach ($avaliacaos as $key => $value) {
            $value = $this->getBasicRelations($value);
        }
       return View::make('avaliacao.listAvaliacao')->with('avaliacaos', $avaliacaos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeOne($avaliacao){
        if ($avaliacao instanceof Avaliacao 
                && $avaliacao->professor_id > 0
                && $avaliacao->disciplina_id > 0
                && $avaliacao->evento_id > 0
                && $avaliacao->curso_id > 0){
            $avaliacao->save();
        }
        return $avaliacao;
    }
}