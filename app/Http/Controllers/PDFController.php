<?php

namespace App\Http\Controllers;

use Response;
use Storage;
use App;
use App\User;
use App\Http\Controllers\Controller;

class PDFController extends Controller {

    public function getReadMeUser(){
        $filename = 'readme/ReadMeUser.pdf';
        $path = storage_path($filename);

        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }

    public function generatePDF($tokenArray) {
        $tokenArray = range(1,1350);
        $this->savePDF($this->generatePDFContent($this->parsePDFContent($tokenArray)));
    }

    private function parsePDFContent($tokenArray) {
        return view('pdfGen', ['tokenArray' => $tokenArray]);
    }

    private function generatePDFContent($html) {
        $pdf = App::make('dompdf.wrapper');
        // Por padrao ele ja faz em A4 e retrato
        $pdf->setWarnings(false);

        $pdf->loadHTML($html);
        return $pdf->output();
	}

    private function savePDF($pdf) {
		$fileContent = "";
		$fileContent .= $pdf;

		Storage::disk('pdf')->put('Senhas para o SurveyCECE ('.time().').pdf', $fileContent);
	}

}

?>
