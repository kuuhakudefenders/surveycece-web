<?php

namespace App\Http\Controllers;

use File;
use View;
use App;
use Storage;
use App\User;
use App\Model\Curso;
use App\Model\Token;
use App\Model\Evento;
use App\Model\Formulario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ValidateResponsesController extends Controller {
	
	public function validateResponse(Request $request, $idFormulario) {
		$fileName = $this->makeFileName($idFormulario);

		$csvResponses = $request->file;

		$responseTokensArray = $this->readAnswersToken($csvResponses);
		$validTokensArray = $this->readValidToken($idFormulario);

		$validResponseArray = $this->validateTokens($responseTokensArray,$validTokensArray);

		$arrayAnswer = $this->readAnswers($csvResponses);
		$fileContent = $this->createValidationFile($validResponseArray, $arrayAnswer);

		Storage::disk('validations')->put($fileName.'.csv', $fileContent);

		
		$formulario = Formulario::find($idFormulario);
		$evento = Evento::find($formulario->evento_id);
		$curso = Curso::find($formulario->curso_id);

		app('App\Http\Controllers\ParserRespostasController')->parserFile($evento->id, $curso->id, $fileContent);
	}

	private function makeFileName($idFormulario) {
		$fileName = 'Respostas SurveyCECE - ';
		$formulario = Formulario::find($idFormulario);
		$evento = Evento::find($formulario->evento_id);
		$curso = Curso::find($formulario->curso_id);

		$fileName .= $evento->nome;
		$fileName .= " - ";
		$fileName .= $curso->nome;
		$fileName = str_replace("/","-", $fileName);
		return $fileName;
	}

	private function readAnswersToken($fileName) {
		$arrayAnswerToken = array();
		$fileAnswer = file_get_contents($fileName);

		// Gambiarra => remove as duas primeiras linhas
        $tokenPosition = strpos($fileAnswer, "\n");
		$fileAnswer = substr($fileAnswer, $tokenPosition+1);
		$tokenPosition = strpos($fileAnswer, "\n");
		$fileAnswer = substr($fileAnswer, $tokenPosition+1);

		while (strpos($fileAnswer, "\n") !== false ) {
			$newEnd = strpos($fileAnswer, "\n");
			$line = substr($fileAnswer,0,$newEnd);
	        $arrayAnswerToken[] = $this->getToken($line);
			$fileAnswer = substr($fileAnswer, $newEnd+1);
		}

		if (strpos($fileAnswer, ",") !== false) {
			$arrayAnswerToken[] = $this->getToken($fileAnswer);
		}

        return $arrayAnswerToken;
	}

	private function getToken($line){
		$tokenPosition = strrpos($line, ",");
        $token = substr($line, $tokenPosition+1,5); // 5 is the token size

		return $token;
	}

	private function readAnswers($fileName) {
		$arrayAnswer = array();
		$fileAnswer = file_get_contents($fileName);

		// Gambiarra => remove as duas primeiras linhas
        $tokenPosition = strpos($fileAnswer, "\n");
		$arrayAnswer[] = substr($fileAnswer, 0, $tokenPosition);
		$fileAnswer = substr($fileAnswer, $tokenPosition+1);

		$tokenPosition = strpos($fileAnswer, "\n");
		$arrayAnswer[] = substr($fileAnswer, 0, $tokenPosition);
		$fileAnswer = substr($fileAnswer, $tokenPosition+1);

		while (strpos($fileAnswer, "\n") !== false ) {
			$newEnd = strpos($fileAnswer, "\n");
			$line = substr($fileAnswer,0,$newEnd);
	        $arrayAnswer[] = $line;
			$fileAnswer = substr($fileAnswer, $newEnd+1);
		}
		$arrayAnswer[] = $fileAnswer;
        return $arrayAnswer;
	}

	private function readValidToken($idFormulario) {
		$tokenArray = array();
		$tokens = Token::where('formulario_id', $idFormulario)->get();
		foreach ($tokens as $value) {
			$tokenArray[] = $value->valor;
		}
        return $tokenArray;
	}

	private function createValidationFile($tokenArray, $arrayAnswer) {
		$fileContent = $arrayAnswer[0]."\n".$arrayAnswer[1]."\n";

		foreach ($tokenArray as $it => $value) {
			$fileContent .= $arrayAnswer[$it+2]."\n";
		}
		return $fileContent;
	}

	public function download($idFormulario){
		$fileName = $this->makeFileName($idFormulario);
		$fullPath = Storage::disk('validations')->getDriver()->getAdapter()->getPathPrefix().$fileName.'.csv';
		return response()->download($fullPath);
	}

	private function validateTokens($arrayAnswerToken,$arrayValidToken) {
		$correctAnswer = array();
		foreach ($arrayAnswerToken as $it => $token) {
			if ( ! in_array($token,$arrayValidToken) ||
				 in_array($token,$correctAnswer) ) {
				unset( $arrayAnswerToken[$it] );
			}
			else {
				$correctAnswer[$it] = $token;
			}
		}
		return $correctAnswer;
	}
}

?>
