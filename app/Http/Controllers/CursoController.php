<?php

namespace App\Http\Controllers;

use App\Model\Disciplina;
use App\Model\Curso;
use App\Model\Professor;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Response;

class CursoController extends Controller{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        $curso = Curso::find($id);
        $curso->disciplinas();

        foreach ($curso->disciplinas as $key => $disciplina) {
            $disciplina->professor;
        }

        return View::make('curso.showCurso')->with('curso', $curso);
    }

    public function showOne($id){
        return Curso::find($id);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $cursos =  Curso::orderBy('nome')->get();
        return View::make('curso.listCurso')->with('cursos', $cursos);
    }

    public function indexList(){
        $cursos =  Curso::orderBy('nome')->get();
        return $cursos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
         return View::make('curso.createCurso');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(){
        $rules = array(
            'nome'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::to('cursos/create')->withErrors($validator);
        } else {
            $curso = new Curso;
            $curso->nome = Input::get('nome');
            $curso->save();

            return Redirect::to('cursos');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        // get the nerd
        $curso = Curso::find($id);
        if($curso  !== NULL && !$curso->trashed()){
            // show the edit form and pass the nerd
            return View::make('curso.editCurso')->with('curso', $curso);
        } else{
            return Redirect::to('cursos');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id){
        $rules = array(
            'nome'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return Redirect::to('cursos/' . $id . '/edit')->withErrors($validator);
        } else {
            $curso = Curso::find($id);
            $curso->nome = Input::get('nome');
            $curso->save();

            return $curso;
        }
    }
}