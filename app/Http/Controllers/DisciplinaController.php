<?php

namespace App\Http\Controllers;

use App\Model\Disciplina;
use App\Model\professor;
use App\Http\Controllers\Controller;
use View;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Response;

class DisciplinaController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $disciplinas =  Disciplina::all();
        $professors = array();
        foreach ($disciplinas as $key => $disciplina) {
            $professors[$key] = Professor::where('id',$disciplina->professor_id)->get()->first();
        }

        return View::make('disciplina.listDisciplina', compact('disciplinas', 'professors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeOne($disciplina){ // aceito sugestoes de nome :<

        if ($disciplina instanceof Disciplina 
                && !empty($disciplina->nome) 
                && $disciplina->professor_id > 0 
                && $disciplina->curso_id > 0){

            $result = DB::table('disciplina')->where('nome', $disciplina->nome);
            if($result->first()){
                $disciplina->id = $result->first()->id;
            } else{
                $disciplina->save();
            }
        }
        return $disciplina;
    }
}