<?php

namespace App\Http\Controllers;

use App\Model\Professor;
use App\Http\Controllers\Controller;
use View;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Response;

class ProfessorController extends Controller{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        $professor = Professor::find($id);
        $professor->disciplinas;
        return View::make('professor.showProfessor')->with('professor', $professor);
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $professors =  Professor::all();
        return View::make('professor.listProfessor')->with('professors', $professors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeOne($professor){ // aceito sugestoes de nome :<
        
        if ($professor instanceof Professor &&
            !empty($professor->nome) ) {
            $result = DB::table('professor')->where('nome', $professor->nome);
            if($result->first()){
                $professor->id = $result->first()->id;
            } else{
                $professor->save();
            }
            
        }
        return $professor;
    }
}