<?php

namespace App\Http\Controllers;

use Storage;
use Session;
use Redirect;
use App;
use App\User;
use App\Model\Curso;
use App\Model\Token;
use App\Model\Evento;
use App\Model\Formulario;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Http\Response;


class TokenController extends Controller {

	public function show($idFormulario) {
		$tokenArray = array();
		$tokens = Token::where('formulario_id', $idFormulario)->get();
		foreach ($tokens as $value) {
			$tokenArray[] = $value->valor;
		}
		$fileName = $this->makeFileName($idFormulario);
		$pdf = $this->generatePDF($tokenArray);
		return $this->stream($pdf)->stream($fileName);
	}

	public function store($idFormulario, $tokenAmount) {
		ini_set('memory_limit','1024M');
		$tokenArray = $this->generateToken($tokenAmount, 5);
		foreach ($tokenArray as $key => $value) {
			// token
	        $token = new Token;
	        $token->valor = $value;
	        $token->formulario_id = $idFormulario;
	        $token->save();
		}
	}

	public function download($idFormulario) {
		$tokenArray = array();
		$tokens = Token::where('formulario_id', $idFormulario)->get();
		foreach ($tokens as $value) {
			$tokenArray[] = $value->valor;
		}
		$fileName = $this->makeFileName($idFormulario);
		$pdf = $this->generatePDF($tokenArray);
		return $this->stream($pdf)->download($fileName);
	}

	private function makeFileName($idFormulario) {
		$fileName = 'Senhas SurveyCECE - ';
		$formulario = Formulario::find($idFormulario);
		$evento = Evento::find($formulario->evento_id);
		$curso = Curso::find($formulario->curso_id);

		$fileName .= trim($evento->nome);
		$fileName .= " - ";
		$fileName .= trim($curso->nome);

		return $fileName;
	}

	private function getToken($tokenLength) {
	    $token = "";
	    $codeAlphabet = "ABCDEFGHJKMNPQRSTUVWXYZ";
	    $codeAlphabet.= "abcdefghjkmnpqrstuvwxyz";
	    $codeAlphabet.= "123456789";
	    $max = strlen($codeAlphabet);

	    for ($i=0; $i < $tokenLength; $i++) {
			$rand = rand();
	        $token .= $codeAlphabet[$rand % strlen($codeAlphabet)];
	    }

	    return $token;
	}

	private function generateToken($tokenAmount, $tokenLength) {
		$tokenArray = array();

		if($tokenAmount > 10000){
			echo "Quantidade máxima de tokens permitidos para a geração é 10k.\n";
		} else {
			for ($i = 0; $i <= $tokenAmount -1; $i++){
				$tokenArray[$i] = $this->getToken($tokenLength);
			}
		}
		return $tokenArray;
	}

    private function generatePDF($tokenArray) {
		return view('evento/tokenPDF', ['tokenArray' => $tokenArray]);
    }

    private function stream($html) {
        $pdf = App::make('dompdf.wrapper');
        $pdf->setWarnings(false);
        $pdf->loadHTML($html);
        return $pdf;
	}

    private function savePDF($pdf, $fileName) {
		$fileContent = "";
		$fileContent .= $pdf;

        Storage::disk('pdf')->put($fileName.'.pdf', $fileContent);
	}

}

?>
