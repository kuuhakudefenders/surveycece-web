<?php

namespace App\Http\Controllers;

use App\Model\Evento;
use App\Model\Curso;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Subscribed;

class EventoController extends Controller{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        $evento = Evento::find($id);
        $formularios = $evento->formularios()->where(array(
            ['evento_id', $evento->id],
        ))->get();
        
        foreach ($formularios as $key => $formulario) {
            $curso = Curso::where('id', $formulario->curso_id)->get();
            $formulario->curso = $curso;
        }
        $evento->formularios = $formularios;
        return View::make('evento.event-open')->with('evento', $evento);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $listEventoAberto = Evento::where('abertoSN', 1)
               ->orderBy('dt_inicio', 'desc')
               ->get();
        $listEventoFechado = Evento::where('abertoSN', 0)
               ->orderBy('dt_fim', 'desc')
               ->get();
        $listEvento =  $listEventoAberto->merge($listEventoFechado);
        return View::make('evento.events-home')->with('eventos', $listEvento);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
         return View::make('evento.event-new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(){
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'nome'       => 'required',
            'dt_inicio'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        
        // process the login
        if ($validator->fails()) {
            return Redirect::to('eventos/create')->withErrors($validator);
        } else {
            
            // store
            $evento = new Evento;
            $evento->nome = Input::get('nome');
            $evento->dt_inicio = Input::get('dt_inicio');
            $evento->descricao = Input::get('descricao');
            $evento->save();

            // redirect
            Session::flash('message', 'Evento criado com sucesso!');
            return Redirect::to('eventos');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        $evento = Evento::find($id);
        if($evento->abertoSN == 1 ){
            return View::make('evento.editEvento')->with('evento', $evento);
        } else{
            return Redirect::to('eventos');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id){
        $rules = array(
            'nome'       => 'required',
            'dt_inicio'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('eventos/' . $id . '/edit')->withErrors($validator);
        } else {
            $evento = Evento::find($id);
            $evento->nome = Input::get('nome');
            $evento->dt_inicio = Input::get('dt_inicio');
            $evento->descricao = Input::get('descricao');
            $evento->save();

            Session::flash('message', 'Evento atualizado com sucesso!');
            return Redirect::to('eventos');
        }
    }

    /**
     * Close the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function close($id){
        $evento = Evento::find($id);
        if ($evento !== null) {
            
            $evento->abertoSN = false;
            $carbon_today= Carbon::today();
            $evento->dt_fim = $carbon_today->format('Y-m-d');
            $evento->save();

            Session::flash('message', 'Evento fechado com sucesso!');
            return Redirect::to('eventos');
        } else{
            return Redirect::to('eventos');
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        $evento = Evento::find($id);
        $evento->delete();

        Session::flash('message', 'Evento removido com sucesso!');
        return Redirect::to('eventos');
    }

    
}