<?php

namespace App\Http\Controllers;

use App;
use App\Model\Curso;
use App\Model\Evento;
use App\Model\Disciplina;
use App\Model\Professor;
use App\Http\Controllers\Controller;
use View;

class ReportController extends Controller{
    private function getEventos(){
       $listEventoFechado = Evento::where('abertoSN', 0)
               ->orderBy('dt_fim', 'desc')
               ->get(); 

        return $listEventoFechado;
    }

    public function indexEventos(){
        return View::make('relatorio/reports-home')->with('listEventoFechado', $this->getEventos());
    }

    private function getCursos($id){
        $evento = Evento::find($id);
        $formularios = $evento->formularios()->where(array(
            ['evento_id', $evento->id],
        ))->get();
        
        $cursosList = array();
        foreach ($formularios as $key => $formulario) {
            $cursos = Curso::where('id', $formulario->curso_id)->get();
            foreach ($cursos as $key => $curso) { 
                array_push($cursosList, $curso);
            }
        }

        return $cursosList;
    }

    public function indexCursos($id){
        $listEventoFechado = $this->getEventos();
        $eventoSelecionado = $id;
        $cursosList = $this->getCursos($id);
        return View::make('relatorio/reports-home', compact('cursosList', 'listEventoFechado','eventoSelecionado'));
    }

    public function getProfessors($evento_id, $curso_id){
        $curso = Curso::find($curso_id);
        $disciplinas = $curso->disciplinas()->where(array(
            ['curso_id', $curso->id],
        ))->get();

        $professorsList = array();
        foreach ($disciplinas as $key => $disciplina) {
            $professors = Professor::where('id', $disciplina->professor_id)->get();
            foreach ($professors as $key => $professor) { 
                array_push($professorsList, $professor);
            }
        }
        
        $sameProfessorsList = array();
        foreach ($professorsList as $key => $professor) { 
             for ($i=$key + 1; $i < count ($professorsList); $i++) { 
                 if ($professor->nome === $professorsList[$i]->nome) {
                     array_push($sameProfessorsList, $i);
                 }
             }
        }

        foreach ($sameProfessorsList as $key => $value) {
            unset($professorsList[$value]);
        }
        
        return $professorsList;
    }

    public function indexProfessors($evento_id, $curso_id){
        $listEventoFechado = $this->getEventos();
        $eventoSelecionado = $evento_id;
        $cursosList = $this->getCursos($evento_id);
        $cursoSelecionado = $curso_id;
        $professorsList = $this->getProfessors($evento_id,$curso_id);
        return View::make('relatorio/reports-home', compact('cursosList', 'listEventoFechado', 'eventoSelecionado', 'cursoSelecionado','professorsList'));
    }

    

    private function generatePDFRelatorioEspecifico($evento_id, $curso_id, $professor_id){
        $avaliacaos = app('App\Http\Controllers\AvaliacaoController')->getAvaliacaoEspecifica($evento_id, $curso_id, $professor_id);
        
        $questions = app('App\Http\Controllers\AvaliacaoController')->getQuestions($avaliacaos[0]->respostas);
        $evento = $avaliacaos[0]->evento()->first();
        $curso = $avaliacaos[0]->curso()->first();
        
        return view('relatorio/especific-professor-report',compact('avaliacaos','questions','evento','curso'));
    }


    private function generatePDFRelatorioGeral($evento_id, $curso_id, $professor_id){
        $avaliacaos = app('App\Http\Controllers\AvaliacaoController')->getAvaliacaoFromEvento($evento_id);
        $perguntas = array();
        
        $evento = $avaliacaos[0]->evento()->first();
        $curso = $avaliacaos[0]->curso()->first();

        foreach ($avaliacaos as $key => $avaliacao) {
            if ($avaliacao->professor->id != $professor_id) {
                $avaliacao->professor->nome = "XXXXXX";
                $avaliacao->disciplina->nome = "XXXXXX";
            }     
            
            foreach ($avaliacao->respostaEstatistica as $key2 => $value) {               
                for ($i=0; $i < count($perguntas); $i++) { 
                    if ($perguntas[$i]->id == $value->pergunta->id){
                        break;
                    }    
                }
                if($i >= count($perguntas)){
                    array_push($perguntas, $value->pergunta);    
                }
            }
        }
        return view('relatorio/general-professor-report',compact('avaliacaos','perguntas','evento','curso'));
    }

    public function downloadRelatorioGeral($evento_id, $curso_id, $professor_id){

        $evento = Evento::find($evento_id);
        $curso = Curso::find($curso_id);
        $professor = Professor::find($professor_id);
        $fileName = "Relatorio Geral - " . $evento->nome . " - " . $curso->nome . " - " . $professor->nome . ".pdf";
        
        $pdf = $this->generatePDFRelatorioGeral($evento_id, $curso_id, $professor_id);
        return $this->stream($pdf)->download($fileName);
    }

    public function downloadRelatorioEspecifico($evento_id, $curso_id, $professor_id){

        $evento = Evento::find($evento_id);
        $curso = Curso::find($curso_id);
        $professor = Professor::find($professor_id);
        $fileName = "Relatorio Especifico - " . $evento->nome . " - " . $curso->nome . " - " . $professor->nome . ".pdf";
        $pdf = $this->generatePDFRelatorioEspecifico($evento_id, $curso_id, $professor_id);
        return $this->stream($pdf)->download($fileName);
    }

    private function stream($html) {
        $pdf = App::make('dompdf.wrapper');
        $pdf->setWarnings(false);
        $pdf->loadHTML($html);
        return $pdf;
    }
}