<?php

namespace App\Http\Controllers;

use App\Model\Resposta;
use App\Http\Controllers\Controller;
use View;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Response;

class RespostaController extends Controller{
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeOne($resposta){
    
        if ($resposta instanceof Resposta 
                && $resposta->pergunta_id > 0
                && $resposta->avaliacao_id > 0
                && $resposta->notaRecebida >= 0
                && $resposta->notaRecebida <= 10){
            $resposta->save();
        }
        
        return $resposta;
    }
    
}