# **READ ME** #

# Ferramentas Utilizadas#

* [Linguagem de programação PHP v7.0](https://secure.php.net/)
* [Framework Laravel v5.3.30](https://laravel.com/)
* [SGBD MySQL v5.7](https://www.mysql.com/)

# Instalação #

## Clone ##

Clone o repositório para o disco local.

## Instalação de Dependências ##
Execute o comando para instalar as dependências do Laravel ao projeto local
```
#!Bash
composer update
```

## Configuração da máquina local ##

### Com Homestead ###

Caso os desenvolvedores estejam utilizando o [Homestead](https://laravel.com/docs/5.4/homestead), as configurações locais da Máquina Virtual devem ser a mesma. 

Neste caso, o arquivo .env padrão já esta configurado para o uso. Tornando-se necessário a instalação das ferramentas adequadas e a sua configuração. Para maiores detalhes, verifique a documentação oficial.

### Sem Homestead ###

Duplique o arquivo ".env.example" e renomeie para ".env"

**IMPORTANTE: Não remova o arquivo original, pois este é necessário para a configuração do projeto em novas máquinas** 

No arquivo .env, insira as informações da máquina local. Cada máquina local deve possuir o seu próprio .env com as suas próprias configurações (para esta aplicação, apenas as configurações relacionada ao banco de dados são necessárias).

Execute os comandos:
```
#!Bash
php artisan key:generate
```
```
#!Bash
php artisan config:clear
```

## Executar a aplicação ##

### Sem Homestead ###

Para iniciar a aplicação, execute:

```
#!Bash
php artisan serve
```

Abra a aplicação no browser com a URL localhost:8000

### Com Homestead ###

Acesse o IP/URL selecionado na etapa de configurações