	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'], function(){
   	// Rota para manual do usuário
   	Route::get('readmeUser', 'PDFController@getReadMeUser');

   	//Rotas usadas nos eventos
   	Route::resource('eventos', 'EventoController');
   	Route::resource('formularios', 'FormularioController');
   	Route::get('eventos/close/{id}', 'EventoController@close');

   	Route::get('/event-new', function () {
		return view('event-new');
	})->middleware('auth');

	// Rotas usadas nos formularios dos eventos
	Route::get('cursosList', 'CursoController@indexList');
   	Route::get('tokens/download/{idFormulario}', 'TokenController@download');
   	Route::get('tokens/show/{idFormulario}', 'TokenController@show');
	Route::get('validate-responses/download/{idFormulario}', 'ValidateResponsesController@download');
	Route::post('tokens/store/{idFormulario}/{tokenAmount}', 'TokenController@store');
	Route::post('validate-responses/{idFormulario}', 'ValidateResponsesController@validateResponse');
	Route::post('formularios/{id}/adicionarLinkFormulario', 'FormularioController@addLinkGoogleForm');

   	Route::get('generate-tokens', function () {
	    return view('generate-tokens');
	});

	Route::get('/pdf', function () {
	    return view('pdf');
	});

	Route::get('validate', array(
		'as' =>'validate',
		'uses'=> 'ValidateResponsesController@getView'
	));

	// Rotas para os relatórios
	Route::get('reports/eventos/{evento_id}/cursos/{curso_id}', 'ReportController@indexProfessors');
	Route::get('reports/eventos', 'ReportController@indexEventos');
	Route::get('reports/eventos/{id}', 'ReportController@indexCursos');
	Route::get('reports/download', 'ReportController@download');
	Route::get('reports/download/{evento_id}/{professor_id}','ReportController@indexAvaliacaoGeral');
	Route::get('reports/eventos/{evento_id}/cursos/{curso_id}/professors/{professor_id}/downloadRelatorioGeral','ReportController@downloadRelatorioGeral');
	Route::get('reports/eventos/{evento_id}/cursos/{curso_id}/professors/{professor_id}/downloadRelatorioEspecifico','ReportController@downloadRelatorioEspecifico');
	
	Route::get('reports', function() {
		return view('reports-home');
	});

	// Rotas para os cursos
	Route::resource('cursos', 'CursoController');

	// Rotas para de listagem geral (Uso apenas para desenvolvimento apenas)
	// Route::resource('resources', 'ResourceController');
 	// Route::resource('professors', 'ProfessorController');
 	// Route::resource('disciplinas', 'DisciplinaController');
	// Route::resource('perguntas', 'PerguntaController');
	// Route::resource('avaliacaos', 'AvaliacaoController');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/', function () {
    return view('welcome');
});